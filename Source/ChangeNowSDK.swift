//
//  ChangeNowSDK.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public class ChangeNowSDK {
    
    public static var localization = Localization(forceLanguageLocalization: nil)
}

extension ChangeNowSDK {
    public struct Localization {
        
        public enum Language: String {
            case english = "en"
        }

        public let forceLanguage: Language?
        
        public init(forceLanguageLocalization forceLanguage: Language?) {
            self.forceLanguage = forceLanguage
        }
    }
}
