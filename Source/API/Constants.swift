//
//  Constants.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

enum CNConstants {
    static let cnApiBaseUrl = URL(string: "https://api.changenow.io/v2")
}

internal let apiDateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    formatter.timeZone = .UTC
    return formatter
}()

internal extension TimeZone {

    static var UTC: TimeZone {
        return TimeZone(abbreviation: "UTC")!
    }

    static var GMT: TimeZone {
        return TimeZone(secondsFromGMT: 0)!
    }
}
