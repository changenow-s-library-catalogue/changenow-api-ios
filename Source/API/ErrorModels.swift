//
//  ErrorModels.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 31.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public enum CNNetworkError: Error {
    case jsonDecoder(Error)
    case underlying(Error)
    case api(error: ApiError)
    case emptyResult
    case unown
    
    public struct ApiError {
        
        public let response: URLResponse
        public let responseData: Data?
        
        public let errorType: String?
        public let message: String?
        
        init(response: URLResponse, responseData: Data?) {
            self.response = response
            self.responseData = responseData
            
            if let responseData = responseData,
               let jsonObject = try? JSONSerialization.jsonObject(with: responseData, options: []) as? [String: String] {
                self.message = jsonObject["message"]
                self.errorType = jsonObject["error"]
            } else {
                self.message = nil
                self.errorType = nil
            }
        }
    }
}
