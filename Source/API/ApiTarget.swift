//
//  ApiTarget.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 29.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

enum ApiTarget {
    case availableCurrencies(active: Bool?,
                             flow: CNExchangeFlow?,
                             buy: Bool?,
                             sell: Bool?)
    
    case minimalExchangeAmount(fromCurrency: String,
                               toCurrency: String,
                               fromNetwork: String?,
                               toNetwork: String?,
                               flow: CNExchangeFlow?)
    
    case exchangeRange(fromCurrency: String,
                       toCurrency: String,
                       fromNetwork: String?,
                       toNetwork: String?,
                       flow: CNExchangeFlow?)
    
    case estimatedExchangeAmount(fromCurrency: String,
                                 toCurrency: String,
                                 fromNetwork: String?,
                                 toNetwork: String?,
                                 flow: CNExchangeFlow,
                                 type: CNExchangeType,
                                 amount: Decimal,
                                 useRateId: Bool)
    
    case createExchange(fromCurrency: String,
                        toCurrency: String,
                        fromNetwork: String?,
                        toNetwork: String?,
                        address: String,
                        addressExtraId: String?,
                        refundAddress: String?,
                        refundExtraId: String?,
                        userId: String?,
                        payload: [String: String]?,
                        contactEmail: String?,
                        flow: CNExchangeFlow,
                        type: CNExchangeType,
                        amount: Decimal,
                        rateId: String?)
    
    case txStatus(id: String)
    
    case addressValidation(currency: String, address: String)
    case userAddressesByName(name: String)
    
    var baseURL: URL {
        CNConstants.cnApiBaseUrl!
    }
    
    var path: String {
        switch self {
        case .availableCurrencies:
            return "/exchange/currencies"
        case .minimalExchangeAmount:
            return "/exchange/min-amount"
        case .exchangeRange:
            return "/exchange/range"
        case .estimatedExchangeAmount:
            return "/exchange/estimated-amount"
        case .createExchange:
            return "/exchange"
        case .txStatus:
            return "/exchange/by-id"
        case .addressValidation:
            return "/validate/address"
        case .userAddressesByName:
            return "/addresses-by-name"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .availableCurrencies, .minimalExchangeAmount, .exchangeRange, .estimatedExchangeAmount, .txStatus, .addressValidation, .userAddressesByName:
            return .get
        case .createExchange:
            return .post
        }
    }
    
    var task: Task {
        switch self {
        case let .availableCurrencies(active, flow, buy, sell):
            
            let parameters: [String: Any] = [
                "active": active ?? "",
                "flow": flow?.rawValue ?? "",
                "buy": buy ?? "",
                "sell": sell ?? ""
            ]
            
            return .requestParameters(parameters: parameters, encoding: URLEncoding(destination: .queryString, boolEncoding: .literal))
            
            
        case let .exchangeRange(fromCurrency, toCurrency, fromNetwork, toNetwork, flow),
             let .minimalExchangeAmount(fromCurrency, toCurrency, fromNetwork, toNetwork, flow):
            
            let parameters: [String: Any] = [
                "flow": flow?.rawValue ?? "",
                "fromCurrency": fromCurrency,
                "toCurrency": toCurrency,
                "fromNetwork": fromNetwork ?? "",
                "toNetwork": toNetwork ?? ""
            ]
            
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
            
        case let .estimatedExchangeAmount(fromCurrency, toCurrency, fromNetwork, toNetwork, flow, type, amount, useRateId):
            
            var parameters: [String: Any] = [
                "flow": flow.rawValue,
                "type": type.rawValue,
                "fromCurrency": fromCurrency,
                "toCurrency": toCurrency,
                "fromNetwork": fromNetwork ?? "",
                "toNetwork": toNetwork ?? "",
                "useRateId": useRateId,
                "source": "mobile_ios_widget"
            ]
            
            switch type {
            case .direct:
                parameters["fromAmount"] = amount
            case .reverse:
                parameters["toAmount"] = amount
            }
            
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
            
        case let .txStatus(id):
            return .requestParameters(parameters: ["id": id], encoding: URLEncoding.queryString)
            
        case let .addressValidation(currency, address):
            return .requestParameters(parameters: ["currency": currency, "address": address], encoding: URLEncoding.queryString)
            
        case let .userAddressesByName(name):
            return .requestParameters(parameters: ["name": name], encoding: URLEncoding.queryString)
            
            
        case let .createExchange(fromCurrency, toCurrency, fromNetwork, toNetwork, address, addressExtraId, refundAddress, refundExtraId, userId, payload, contactEmail, flow, type, amount, rateId):
            
            var parameters: [String: Any] = [
                "flow": flow.rawValue,
                "type": type.rawValue,
                "fromCurrency": fromCurrency,
                "toCurrency": toCurrency,
                "fromNetwork": fromNetwork ?? "",
                "toNetwork": toNetwork ?? "",
                "address": address,
                "extraId": addressExtraId ?? "",
                "refundAddress": refundAddress ?? "",
                "refundExtraId": refundExtraId ?? "",
                "userId": userId ?? "",
                "contactEmail": contactEmail ?? "",
                "payload": payload ?? "",
                "rateId": rateId ?? "",
                "source": "mobile_ios_widget"
            ]
            
            switch type {
            case .direct:
                parameters["fromAmount"] = amount
            case .reverse:
                parameters["toAmount"] = amount
            }
            
            return .requestParameters(parameters: parameters, encoding: JSONEncoding.default)
        }
    }
}
