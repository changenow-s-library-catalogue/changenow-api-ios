//
//  ApiService.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//
import Foundation
import UIKit

//MARK: - ApiService
public final class CNApiService {
    typealias RawResultBlock = (Result<Data?, CNNetworkError>) -> Void
    private let urlSession: URLSession
    
    public init(apiKey: String) {
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30
        sessionConfig.waitsForConnectivity = true
        sessionConfig.httpAdditionalHeaders = ["x-changenow-api-key": apiKey]
        urlSession = URLSession(configuration: sessionConfig)
    }
    
    //MARK: - Available currencies
    
    /// Returns the list of available currencies.
    ///
    /// - Parameters:
    ///   - onlyActive: Set `true` to return only active currencies.
    ///   - flow:       Type of exchange flow.
    ///   - withBuy:    If this field is `true`, only currencies available for buy are returned.
    ///   - withSell:   If this field is `true`, only currencies available for sell are returned.
    ///
    @discardableResult
    public func fetchAvailableCurrencies(onlyActive: Bool? = nil,
                                         flow: CNExchangeFlow = .standard,
                                         withBuy: Bool? = nil,
                                         withSell: Bool? = nil,
                                         completion: @escaping (Result<[CNCurrency], CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return request(target: .availableCurrencies(active: onlyActive, flow: flow, buy: withBuy, sell: withSell)) {
            
            let result: Result<[OptionalObject<CNCurrency>], CNNetworkError> = $0.mapToModel()
            let mapedResult = result.map { result in result.compactMap { optionalCurrenct in optionalCurrenct.value } }
            completion(mapedResult)
        }
    }
    
    //MARK: - Minimal exchange amount
    
    /// Returns minimal payment amount required to make an exchange. If you try to exchange less, the transaction will most likely fail.
    ///
    /// - Parameters:
    ///   - fromCurrency: **Currency** you want to exchange.
    ///   - toCurrency:   **Currency** you want to receive.
    ///   - flow:          Type of exchange flow
    ///
    @discardableResult
    public func fetchMinimalExchangeAmount(fromCurrency: CNCurrency,
                                           toCurrency: CNCurrency,
                                           flow: CNExchangeFlow = .standard,
                                           completion: @escaping (Result<CNMinExchangeAmount, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return fetchMinimalExchangeAmount(fromCurrency: fromCurrency.ticker, toCurrency: toCurrency.ticker, fromNetwork: fromCurrency.network, toNetwork: toCurrency.network, flow: flow, completion: completion)
    }
    @discardableResult
    public func fetchMinimalExchangeAmount(fromCurrency: String,
                                           toCurrency: String,
                                           fromNetwork: String? = nil,
                                           toNetwork: String? = nil,
                                           flow: CNExchangeFlow = .standard,
                                           completion: @escaping (Result<CNMinExchangeAmount, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return request(target: .minimalExchangeAmount(fromCurrency: fromCurrency, toCurrency: toCurrency, fromNetwork: fromNetwork, toNetwork: toNetwork,flow: flow)) {
            
            completion($0.mapToModel())
        }
    }
    
    //MARK: - Exchange range
    
    /// Returns minimal payment amount and maximum payment amount required to make an exchange. If you try to exchange less than minimum or more than maximum, the transaction will most likely fail. Any pair of assets has minimum amount and some of pairs have maximum amount.
    ///
    /// - Parameters:
    ///   - fromCurrency: **Currency** you want to exchange.
    ///   - toCurrency:   **Currency** you want to receive.
    ///   - flow:          Type of exchange flow
    ///
    @discardableResult
    public func fetchExchangeRange(fromCurrency: CNCurrency,
                                   toCurrency: CNCurrency,
                                   flow: CNExchangeFlow = .standard,
                                   completion: @escaping (Result<CNExchangeRange, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return fetchExchangeRange(fromCurrency: fromCurrency.ticker, toCurrency: toCurrency.ticker, fromNetwork: fromCurrency.network, toNetwork: toCurrency.network, flow: flow, completion: completion)
    }
    @discardableResult
    public func fetchExchangeRange(fromCurrency: String,
                                   toCurrency: String,
                                   fromNetwork: String? = nil,
                                   toNetwork: String? = nil,
                                   flow: CNExchangeFlow = .standard,
                                   completion: @escaping (Result<CNExchangeRange, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return request(target: .exchangeRange(fromCurrency: fromCurrency, toCurrency: toCurrency, fromNetwork: fromNetwork, toNetwork: toNetwork, flow: flow)) {
            
            completion($0.mapToModel())
        }
    }
    
    //MARK: - Estimated exchange amount
    
    /// Returns estimated exchange amount for the exchange and some additional fields.
    ///
    /// - Parameters:
    ///   - fromCurrency: **Currency** you want to exchange.
    ///   - toCurrency:   **Currency** you want to receive.
    ///   - flow:          Type of exchange flow
    ///   - type:          Direction of exchange flow.
    ///     * For **.direct**, it calculates how much **toCurrency** you will receive if you send the **amount** of **fromCurrency**.
    ///     ---
    ///     * For **.reverse**, it calculates how much **fromCurrency** you have to send to get the **amount** of **toCurrency**
    ///
    ///   - amount:        Must be greater then `0`.
    ///   - useRateId:     Use **rateId** for **fixedRate** flow. If this field is `true`, you could use returned field **rateId** in next method for creating transaction to freeze estimated amount that you got in this method. Current estimated amount would be valid until time in field **validUntil**
    ///
    @discardableResult
    public func fetchEstimatedExchangeAmount(fromCurrency: CNCurrency,
                                             toCurrency: CNCurrency,
                                             flow: CNExchangeFlow = .standard,
                                             type: CNExchangeType = .direct,
                                             amount: Decimal,
                                             useRateId: Bool = false,
                                             completion: @escaping (Result<CNEstimatedExchange, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return fetchEstimatedExchangeAmount(fromCurrency: fromCurrency.ticker, toCurrency: toCurrency.ticker, fromNetwork: fromCurrency.network, toNetwork: toCurrency.network, flow: flow, type: type, amount: amount, useRateId: useRateId, completion: completion)
    }
    @discardableResult
    public func fetchEstimatedExchangeAmount(fromCurrency: String,
                                             toCurrency: String,
                                             fromNetwork: String? = nil,
                                             toNetwork: String? = nil,
                                             flow: CNExchangeFlow = .standard,
                                             type: CNExchangeType = .direct,
                                             amount: Decimal,
                                             useRateId: Bool = false,
                                             completion: @escaping (Result<CNEstimatedExchange, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return request(target: .estimatedExchangeAmount(fromCurrency: fromCurrency, toCurrency: toCurrency, fromNetwork: fromNetwork, toNetwork: toNetwork, flow: flow, type: type, amount: amount, useRateId: useRateId)) {
            
            completion($0.mapToModel())
        }
    }
    
    //MARK: - Create exchange
    
    /// Creates a transaction, generates an address for sending funds and returns transaction attributes.
    ///
    /// - Note: We also give the opportunity to transfer additional fields in this `func`,  which we return in the **fetchTxStatus(...)**
    /// ---
    /// Аdditional fields that can be transferred include:
    ///  * userId — user ID
    ///  ---
    ///  * payload — object that can contain up to 5 arbitrary fields up to 64 characters long
    /// ---
    /// If you would like to enable these fields, please contact us at api@changenow.io with the subject line "Special partner fields".
    ///
    /// - Parameters:
    ///   - fromCurrency:   **Currency**  you want to exchange.
    ///   - toCurrency:     **Currency** you want to receive.
    ///   - address:        The wallet address that will recieve the exchanged funds.
    ///   - addressExtraId: Extra ID of **address** (optional)
    ///   - refundAddress:  Preset refund address (optional)
    ///   - refundExtraId:  Extra ID of **refundAddress** (optional)
    ///   - userId:         User ID (optional)
    ///   - payload:        Object that can contain up to 5 arbitrary fields up to 64 characters long (optional)
    ///   - contactEmail:   Contact email (optional)
    ///   - flow:           Type of exchange flow
    ///   - type:           Direction of exchange flow.
    ///     * For **.direct**, it calculates how much **toCurrency** you will receive if you send the **amount** of **fromCurrency**.
    ///     ---
    ///     * For **.reverse**, it calculates how much **fromCurrency** you have to send to get the **amount** of **toCurrency**
    ///
    ///   - amount:         Must be greater then `0`.
    ///   - rateId:         Use **rateId** for **fixedRate** flow. Taken from **fetchEstimatedExchangeAmount** (optional)
    ///
    @discardableResult
    public func createExchange(fromCurrency: CNCurrency,
                               toCurrency: CNCurrency,
                               address: String,
                               addressExtraId: String? = nil,
                               refundAddress: String? = nil,
                               refundExtraId: String? = nil,
                               userId: String? = nil,
                               payload: [String: String]? = nil,
                               contactEmail: String? = nil,
                               flow: CNExchangeFlow = .standard,
                               type: CNExchangeType = .direct,
                               amount: Decimal,
                               rateId: String? = nil,
                               completion: @escaping (Result<CNTransactionInfo, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return createExchange(fromCurrency: fromCurrency.ticker, toCurrency: toCurrency.ticker, fromNetwork: fromCurrency.network, toNetwork: toCurrency.network, address: address, addressExtraId: addressExtraId, refundAddress: refundAddress, refundExtraId: refundExtraId, userId: userId, payload: payload, contactEmail: contactEmail, flow: flow, type: type, amount: amount, rateId: rateId, completion: completion)
    }
    @discardableResult
    public func createExchange(fromCurrency: String,
                               toCurrency: String,
                               fromNetwork: String? = nil,
                               toNetwork: String? = nil,
                               address: String,
                               addressExtraId: String? = nil,
                               refundAddress: String? = nil,
                               refundExtraId: String? = nil,
                               userId: String? = nil,
                               payload: [String: String]? = nil,
                               contactEmail: String? = nil,
                               flow: CNExchangeFlow = .standard,
                               type: CNExchangeType = .direct,
                               amount: Decimal,
                               rateId: String? = nil,
                               completion: @escaping (Result<CNTransactionInfo, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return request(target: .createExchange(fromCurrency: fromCurrency, toCurrency: toCurrency, fromNetwork: fromNetwork, toNetwork: toNetwork, address: address, addressExtraId: addressExtraId, refundAddress: refundAddress, refundExtraId: refundExtraId, userId: userId, payload: payload, contactEmail: contactEmail, flow: flow, type: type, amount: amount, rateId: rateId)) {
            
            completion($0.mapToModel())
        }
    }
    
    //MARK: - Transaction status
    
    /// Returns the status and additional information of a single transaction. Transaction ID is taken from the **createExchange**.
    ///
    ///- Note: We also give the opportunity to transfer additional fields in the **createExchange(...)**, which we return in this func.
    /// ---
    /// Аdditional fields that can be transferred include:
    ///  * userId — user ID
    ///  ---
    ///  * payload — object that can contain up to 5 arbitrary fields up to 64 characters long
    /// ---
    /// If you would like to enable these fields, please contact us at api@changenow.io with the subject line "Special partner fields".
    ///
    /// - Parameters:
    ///   - id:  Transaction ID
    ///
    @discardableResult
    public func fetchTxStatus(id: String,
                              completion: @escaping (Result<CNTransactionStatus, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return request(target: .txStatus(id: id)) {
            
            completion($0.mapToModel())
        }
    }
    
    //MARK: - Address validation
    
    /// Validates the address with a checksum depending on a transferred network.
    ///
    /// - Parameters:
    ///   - currency:  **Currency** of address
    ///   - address:  Address for validation
    ///
    @discardableResult
    public func addressValidation(currency: CNCurrency,
                                  address: String,
                                  completion: @escaping (Result<CNAddressValidationResult, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return addressValidation(network: currency.network, address: address, completion: completion)
    }
    @discardableResult
    public func addressValidation(network: String,
                                  address: String,
                                  completion: @escaping (Result<CNAddressValidationResult, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return request(target: .addressValidation(currency: network, address: address)) {
            
            completion($0.mapToModel())
        }
    }
    
    //MARK: - User addresses by name
    
    /// Returns a list of addresses bound to address name.
    ///
    /// - Parameters:
    ///   - name:  FIO address or Unstoppable domain as name.zil / name.crypto
    ///
    @discardableResult
    public func userAddressesByName(name: String,
                                    completion: @escaping (Result<CNAddressesByNameResult, CNNetworkError>) -> Void) -> URLSessionDataTask? {
        
        return request(target: .userAddressesByName(name: name)) {
            
            completion($0.mapToModel())
        }
    }
    
    //MARK: - Private
    private func request(target: ApiTarget, completion: @escaping RawResultBlock) -> URLSessionDataTask? {
        var request = URLRequest(url: target.baseURL.appendingPathComponent(target.path))
        request.httpMethod = target.method.rawValue
        
        guard let request = target.task.encode(request) else {
            completion(.failure(.unown))
            return nil
        }
        
        let dataTask = urlSession.dataTask(with: request) { data, response, error in
            if let error = error {
                completion(.failure(.underlying(error)))
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                completion(.failure(.unown))
                return
            }
            
            guard (200..<300).contains(response.statusCode) else {
                completion(.failure(.api(error: .init(response: response, responseData: data))))
                return
            }
            
            completion(.success(data))
        }
        dataTask.resume()
        return dataTask
    }
}

//MARK: - ExchangeFlow & ExchangeType
public enum CNExchangeFlow: String, Codable {
    case standard = "standard"
    case fixedRate = "fixed-rate"
}

public enum CNExchangeType: String, Codable {
    case direct
    case reverse
}

//MARK: - Internal Result extension
extension Result where Success == Data?, Failure == CNNetworkError {
    func mapToModel<Model: Decodable>() -> Result<Model, CNNetworkError> {
        switch self {
        case .success(let data):
            guard let data = data else {
                return .failure(.emptyResult)
            }
            do {
                let jsonDecoder = JSONDecoder()
                let model = try jsonDecoder.decode(Model.self, from: data)
                return .success(model)
            } catch {
                return .failure(.jsonDecoder(error))
            }
        case .failure(let error):
            return .failure(error)
        }
    }
}


