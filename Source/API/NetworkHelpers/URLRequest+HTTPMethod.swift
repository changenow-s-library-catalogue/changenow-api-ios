//
//  URLRequest+HTTPMethod.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

extension URLRequest {
    /// Returns the `httpMethod` as Alamofire's `HTTPMethod` type.
    var method: HTTPMethod? {
        get { httpMethod.flatMap(HTTPMethod.init) }
        set { httpMethod = newValue?.rawValue }
    }
}
