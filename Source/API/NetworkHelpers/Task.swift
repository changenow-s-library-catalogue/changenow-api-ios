//
//  Task.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

/// Represents an HTTP task.
enum Task {

    /// A request with no additional data.
    case requestPlain

    /// A requests body set with encoded parameters.
    case requestParameters(parameters: [String: Any], encoding: ParameterEncoding)
    
    func encode(_ urlRequest: URLRequest) -> URLRequest? {
        switch self {
        case .requestPlain:
            return urlRequest
        case .requestParameters(let parameters, let encoding):
            return encoding.encode(urlRequest, with: parameters)
        }
    }
}
