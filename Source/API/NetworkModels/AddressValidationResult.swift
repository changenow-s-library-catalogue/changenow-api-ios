//
//  AddressValidationResult.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 16.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public struct CNAddressValidationResult: Codable {
    /// Result of validation
    public let result: Bool
    /// Error message
    public let message: String?
}
