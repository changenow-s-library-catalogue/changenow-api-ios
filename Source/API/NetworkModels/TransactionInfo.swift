//
//  TransactionInfo.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 29.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public struct CNTransactionInfo: Codable {
    ///You can use it to get transaction status at the `ApiService.fetchTxStatus`
    public let id: String
    
    public let fromAmount: Decimal
    public let toAmount: Decimal
    public let flow: CNExchangeFlow
    public let type: CNExchangeType
    
    public let fromCurrency: String
    public let fromNetwork: String
    
    public let toCurrency: String
    public let toNetwork: String
    
    ///We generate it when creating a transaction
    public let payinAddress: String
    ///The wallet address that will recieve the exchanged funds
    public let payoutAddress: String
    
    ///We generate it when creating a transaction
    public let payinExtraId: String?
    ///Extra ID that you send when creating a transaction
    public let payoutExtraId: String?
    
    ///Refund address (if you specified it)
    public let refundAddress: String?
    ///Refund Extra ID (if you specified it)
    public let refundExtraId: String?
    ///Field name currency Extra ID (e.g. Memo, Extra ID)
    public let payoutExtraIdName: String?
}
