//
//  TransactionStatus.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 29.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

///Fields in the struct vary depending on the status and a type of the transaction.
public struct CNTransactionStatus: Codable {
    
    public enum Status: String, Codable {
        case new
        case waiting
        case confirming
        case exchanging
        case sending
        case finished
        case failed
        case refunded
        case verifying
    }
    
    public let id: String
    
    public let status: Status
    
    ///Indicates if an exchange can be pushed or refunded using Public push & refund endpoints
    ///
    ///https://documenter.getpostman.com/view/8180765/TzJoFLtG#acf2515b-99c7-44bd-935c-dc42693b8026
    public let actionsAvailable: Bool?
    
    public let fromCurrency: String
    public let fromNetwork: String
    
    public let toCurrency: String
    public let toNetwork: String
    
    ///The amount you want to send
    public let expectedAmountFrom: Decimal?
    
    ///Estimated value that you will get based on the field expectedAmountFrom
    public let expectedAmountTo: Decimal?
    
    ///Exchange amount of fromCurrency
    ///
    ///Сompleted after confirmation of the deposit
    public let amountFrom: Decimal?
    
    ///Exchange amount of toCurrency
    ///
    ///Сompleted after exchange
    public let amountTo: Decimal?
    
    ///We generate it when creating a transaction
    public let payinAddress: String
    
    ///The wallet address that will recieve the exchanged funds
    public let payoutAddress: String
    
    ///We generate it when creating a transaction
    public let payinExtraId: String?
    ///Extra ID that you send when creating a transaction
    public let payoutExtraId: String?
    
    ///Refund address (if you specified it)
    public let refundAddress: String?
    ///Refund Extra ID (if you specified it)
    public let refundExtraId: String?
    ///Field name currency Extra ID (e.g. Memo, Extra ID)
    public let payoutExtraIdName: String?
    
    ///Transaction creation date and time
    ///
    ///dateFormat is `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`
    public let createdAt: String?
    
    ///Date and time of the last transaction update (e.g. status update)
    ///
    ///dateFormat is `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`
    public let updateAt: String?
    
    ///Exists when a fixed-rate flow is used. Date by which the deposit must be sent
    ///
    ///dateFormat is `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`
    public let validUntil: String?
    
    ///Deposit receiving date and time
    ///
    ///dateFormat is `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`
    public let depositReceivedAt: String?
    
    ///Transaction hash in the blockchain of the currency which you specified in the fromCurrency field that you send when creating the transaction
    public let payinHash: String?
    ///Transaction hash in the blockchain of the currency which you specified in the toCurrency field. We generate it when creating a transaction
    public let payoutHash: String?
    
    ///Ticker of the currency you want to exchange in an old format
    public let fromLegacyTicker: String
    ///Ticker of the currency you want to receive in an old format
    public let toLegacyTicker: String
    
    
    public let userId: String?
    public let payload: [String: String]?
}
