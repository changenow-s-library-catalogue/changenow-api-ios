//
//  Currency.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 29.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public struct CNCurrency: Codable {
    ///Currency normal ticker    e.g. `usdt`
    public let ticker: String
    ///Currency name    e.g. `Tether (ERC20)`
    public let name: String
    ///Currency logo url  (svg)   e.g. `https://changenow.io/images/sprite/currencies/usdterc20.svg`
    public let image: String
    ///Indicates if a currency has an Extra ID
    public let hasExternalId: Bool
    ///Indicates if a currency is a fiat currency (EUR, USD)
    public let isFiat: Bool
    ///Indicates if a currency is popular
    public let featured: Bool
    ///Indicates if a currency is stable
    public let isStable: Bool
    ///Indicates if a currency is available on a fixed-rate flow
    public let supportsFixedRate: Bool
    ///Currency network    e.g. `eth`, if not token - `network` = `tiker`
    public let network: String
    ///Contract for token or `null` for non-token
    public let tokenContract: String?
    ///Indicates if a currency is available to buy
    public let buy: Bool
    ///Indicates if a currency is available to sell
    public let sell: Bool
    ///Currency legacy tiker    e.g. `usdterc20`
    public let legacyTicker: String
}
