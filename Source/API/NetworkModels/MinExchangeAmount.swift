//
//  MinExchangeAmount.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 29.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public struct CNMinExchangeAmount: Codable {
    public let fromCurrency: String
    public let fromNetwork: String
    public let toCurrency: String
    public let toNetwork: String
    public let flow: CNExchangeFlow
    public let minAmount: Decimal
}
