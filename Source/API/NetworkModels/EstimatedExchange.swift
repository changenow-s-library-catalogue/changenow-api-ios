//
//  EstimatedExchange.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 29.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public struct CNEstimatedExchange: Codable {
    public let fromCurrency: String
    public let fromNetwork: String
    
    public let toCurrency: String
    public let toNetwork: String
    
    public let flow: CNExchangeFlow
    public let type: CNExchangeType
    
    ///RateId is needed for `fixed-rate` flow.
    ///
    ///If you set param `useRateId` to true, you could use returned field `rateId` in next method for creating transaction to freeze estimated amount that you got in this method.
    ///Current estimated amount would be valid until time in field `validUntil`
    public let rateId: String?
    
    ///Date and time before estimated amount would be freezed in case of using `rateId`.
    ///
    ///If you set param `useRateId` to true, you could use returned field `rateId` in next method for creating transaction to freeze estimated amount that you got in this method.
    ///Estimated amount would be valid until this date and time
    ///
    ///dateFormat is `yyyy-MM-dd'T'HH:mm:ss.SSS'Z'`
    public let validUntil: String?
    public var validUntilDate: Date? {
        guard let validUntil = validUntil else { return nil }
        return apiDateFormatter.date(from: validUntil)
    }
    
    ///Dash-separated min and max estimated time in minutes
    public let transactionSpeedForecast: String?
    
    ///Some warnings like warnings that transactions on this network take longer or that the currency has moved to another network
    public let warningMessage: String?
    
    ///Exchange amount of `fromCurrency`
    ///
    ///In case when `type`= `.reverse` it is an estimated value
    public let fromAmount: Decimal
    
    ///Exchange amount of `toCurrency`
    ///
    ///In case when `type`= `.direct` it is an estimated value
    public let toAmount: Decimal
}
