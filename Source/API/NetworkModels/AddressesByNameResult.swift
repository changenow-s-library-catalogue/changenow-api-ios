//
//  AddressesByNameResult.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 19.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public struct CNAddressesByNameResult: Codable {
    
    public let success: Bool
    /// Array of `Address` if exist
    public let addresses: [Address]
    /// Error message
    public let message: String?
    /// Error code
    public let error: String?
    
    public struct Address: Codable {
        ///Currency ticker in naming space of his protocol
        public let currency: String
        ///Currency chain in naming space of his protocol
        public let chain: String
        ///Real address for requested fio-address or unstoppable-domain
        public let address: String
        ///Protocol of current address
        public let `protocol`: String
    }
}
