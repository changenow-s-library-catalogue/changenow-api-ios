//
//  CurrencyExchangeField.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 27.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

final class CurrencyExchangeField: UIView {

    // MARK: - Views

    private lazy var fieldContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.font = .regular(ofSize: 12)
        view.textColor = .medium2Gray
        return view
    }()

    private lazy var field: UITextField = {
        let view = UITextField()
        view.adjustsFontSizeToFitWidth = true
        view.font = .regular(ofSize: 24)
        view.tintColor = .cnGreen
        view.keyboardType = .decimalPad
        view.textColor = .mainBlack
        view.addTarget(self, action: #selector(editingDidBeginAction), for: .editingDidBegin)
        view.addTarget(self, action: #selector(editingDidEndAction), for: .editingDidEnd)
        return view
    }()

    private lazy var fieldActivityIndicator: NVActivityIndicatorView = {
        let view = NVActivityIndicatorView(frame: .zero, type: .ballBeat, color: .white, padding: nil)
        view.color =  .cnGreen
        return view
    }()

    private(set) lazy var currencyContainer: CurrencyContainer = {
        let view = CurrencyContainer()
        view.addTarget(self, action: #selector(chooseCurrencyButtonAction), for: .touchUpInside)
        view.set(isActive: false)
        return view
    }()

    // MARK: - Private

    private var isFieldSelected = false
    private var chooseCurrencyAction: (() -> Void)?

    // MARK: - Life Cycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        addSubviews()
        setConstraints()

        layer.cornerRadius = 8
        layer.masksToBounds = true
        
        layer.borderColor = UIColor.mediumPurple.cgColor
        layer.borderWidth = 1
    }

    // MARK: - Public

    override var isFirstResponder: Bool {
        return field.isFirstResponder
    }

    override func becomeFirstResponder() -> Bool {
        field.becomeFirstResponder()
    }

    func set(isEditable: Bool) {
        field.isUserInteractionEnabled = isEditable
        if isEditable {
            fieldContainer.backgroundColor = .white
            field.textColor = .mainBlack
        } else {
            fieldContainer.backgroundColor = .darkPurple
            field.textColor = .white
        }
    }

    func set(fieldDelegate: UITextFieldDelegate,
             chooseCurrencyAction: (() -> Void)?) {
        field.delegate = fieldDelegate
        
        self.chooseCurrencyAction = chooseCurrencyAction
        currencyContainer.set(isActive: chooseCurrencyAction != nil)
    }

    func set(title: String) {
        titleLabel.text = title
    }

    func set(value: Decimal?) {
        if let value = value {
            field.text = "\(value.rounding(withMode: .down, scale: 8))"
            fieldActivityIndicator.stopAnimating()
        } else {
            field.text = ""
            fieldActivityIndicator.startAnimating()
        }
        updateFieldSelection()
    }

    func set(currency: CurrencyType) {
        currencyContainer.set(currency: currency)
    }

    func setFieldSelection(state: Bool) {
        isFieldSelected = state
        UIView.transition(
            with: self,
            duration: 0.3,
            options: .transitionCrossDissolve,
            animations: {
                self.updateFieldSelection()
            },
            completion: nil)
    }
    
    func setWarning() {
        layer.borderColor = UIColor.alertStock.cgColor
        layer.borderWidth = 2
    }
    
    func isSame(textField: UITextField) -> Bool {
        return field == textField
    }

    // MARK: - Private

    private func addSubviews() {
        addSubview(fieldContainer)
        fieldContainer.addSubview(titleLabel)
        fieldContainer.addSubview(field)
        fieldContainer.addSubview(fieldActivityIndicator)
        addSubview(currencyContainer)
    }

    private func setConstraints() {
        
        fieldContainer.makeConstraints {
            [$0.topAnchor.constraint(equalTo: topAnchor),
             $0.bottomAnchor.constraint(equalTo: bottomAnchor),
             $0.leadingAnchor.constraint(equalTo: leadingAnchor),
             $0.trailingAnchor.constraint(equalTo: currencyContainer.leadingAnchor)]
        }
        
        titleLabel.makeConstraints {
            [$0.topAnchor.constraint(equalTo: fieldContainer.topAnchor, constant: 12),
             $0.leadingAnchor.constraint(equalTo: fieldContainer.leadingAnchor, constant: 12),
             $0.trailingAnchor.constraint(equalTo: fieldContainer.trailingAnchor, constant: -12)]
        }
        
        field.makeConstraints {
            [$0.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
             $0.leadingAnchor.constraint(equalTo: fieldContainer.leadingAnchor, constant: 12),
             $0.trailingAnchor.constraint(equalTo: fieldContainer.trailingAnchor)]
        }
        
        fieldActivityIndicator.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
             $0.centerYAnchor.constraint(equalTo: field.centerYAnchor),
             $0.widthAnchor.constraint(equalToConstant: 24),
             $0.heightAnchor.constraint(equalToConstant: 24)]
        }
        
        currencyContainer.makeConstraints {
            [$0.widthAnchor.constraint(equalToConstant: 120),
             $0.topAnchor.constraint(equalTo: topAnchor),
             $0.bottomAnchor.constraint(equalTo: bottomAnchor),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor)]
        }
    }

    private func updateFieldSelection() {
        if isFirstResponder {
            layer.borderColor = UIColor.cnGreen.cgColor
            layer.borderWidth = 2
        } else {
            layer.borderColor = UIColor.mediumPurple.cgColor
            layer.borderWidth = 1
        }
    }

    // MARK: - Actions

    @objc
    private func chooseCurrencyButtonAction() {
        chooseCurrencyAction?()
    }

    @objc
    private func editingDidBeginAction() {
        UIView.transition(with: self,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { self.updateFieldSelection() },
                          completion: nil)
        fieldActivityIndicator.stopAnimating()
    }

    @objc
    private func editingDidEndAction() {
        UIView.transition(with: self,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: { self.updateFieldSelection() },
                          completion: nil)
    }
}

final class CurrencyContainer: HighlightedButton {

    private lazy var currencyImageView = UIImageView()

    private lazy var currencyLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.textColor = .white
        view.font = .regular(ofSize: 24)
        view.adjustsFontSizeToFitWidth = true
        return view
    }()

    private lazy var currencyDescription: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.textColor = .medium2Gray
        view.font = .regular(ofSize: 12)
        return view
    }()

    private lazy var currencyArrow: UIImageView = {
        let view = UIImageView()
        view.image = .carretDown
        return view
    }()
    
    private weak var currencyCancalable: Cancalable?

    init() {
        super.init(frame: .zero)
        commonInit()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    // MARK: - Public

    func set(currency: CurrencyType) {
        currencyLabel.text = currency.tickerForShow
        currencyDescription.text = currency.networkForShow
        
        currencyImageView.image = .currencyPlaceholder
        currencyCancalable?.cancel()
        currencyCancalable = currency.getIcon { [weak self] icon in
            if let icon = icon {
                DispatchQueue.main.async {
                    self?.currencyImageView.image = icon
                }
            }
        }
    }

    func set(isActive: Bool) {
        isEnabled = isActive
        if isActive {
            backgroundColor = .mediumPurple
            currencyArrow.isHidden = false
        } else {
            backgroundColor = .darkBG
            currencyArrow.isHidden = true
        }
    }

    // MARK: - Private

    private func commonInit() {
        addSubviews()
        setConstraints()
        
        backgroundColor = .mediumPurple
        currencyArrow.isHidden = false
    }

    private func addSubviews() {
        addSubview(currencyImageView)
        addSubview(currencyDescription)
        addSubview(currencyLabel)
        addSubview(currencyArrow)
    }

    private func setConstraints() {
        currencyImageView.makeConstraints {
            [$0.centerYAnchor.constraint(equalTo: centerYAnchor),
             $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
             $0.widthAnchor.constraint(equalToConstant: 24),
             $0.heightAnchor.constraint(equalToConstant: 24)]
        }
        
        currencyLabel.makeConstraints {
            [$0.centerYAnchor.constraint(equalTo: centerYAnchor),
             $0.leadingAnchor.constraint(equalTo: currencyImageView.trailingAnchor, constant: 6),
             $0.trailingAnchor.constraint(lessThanOrEqualTo: currencyArrow.leadingAnchor, constant: -6)]
        }
        
        currencyDescription.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: currencyLabel.leadingAnchor),
             $0.bottomAnchor.constraint(equalTo: currencyLabel.topAnchor, constant: 1),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -6)]
        }

        currencyArrow.makeConstraints {
            [$0.centerYAnchor.constraint(equalTo: centerYAnchor),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12),
             $0.widthAnchor.constraint(equalToConstant: 16),
             $0.heightAnchor.constraint(equalToConstant: 16)]
        }
    }
}
