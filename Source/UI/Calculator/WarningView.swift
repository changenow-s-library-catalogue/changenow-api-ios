//
//  WarningView.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 27.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation
import UIKit

final class WarningView: UIControl {

    private lazy var textLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.textColor = .white
        view.font = .medium(ofSize: 12)
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews()
        setConstraints()

        backgroundColor = .alertDarkStock
        layer.cornerRadius = 4
        layer.masksToBounds = true
        layer.borderColor = UIColor.alertStock.cgColor
        layer.borderWidth = 1
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Public

    func set(warning: String) {
        textLabel.text = warning
        setNeedsLayout()
    }

    // MARK: - Private

    private func addSubviews() {
        addSubview(textLabel)
    }

    private func setConstraints() {
        textLabel.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
             $0.centerYAnchor.constraint(equalTo: centerYAnchor)]
        }
    }
}
