//
//  CalculatorView.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 26.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import SVGKit


public final class CNCalculatorView: UIView {
    private lazy var sendExchangeField: CurrencyExchangeField = {
        let view = CurrencyExchangeField(frame: .init(origin: .zero, size: .init(width: 200, height: 72)))
        view.set(title: ChangeNowSDK.localization.currencyExchangeFieldTitleSend())
        view.set(isEditable: true)
        view.set(currency: exchangeManager.sendCurrency)
        view.set(fieldDelegate: self, chooseCurrencyAction: exchangeManager.baseSelectSendCurrencyAction)
        view.set(value: exchangeManager.amount)
        return view
    }()
    
    private lazy var rateView: CurrencyExchangeRateView = {
        let view = CurrencyExchangeRateView(frame: .init(origin: .zero, size: .init(width: 200, height: 36)))
        view.warninTapAction = { [weak self] type in
            self?.sendExchangeField.set(value: type.amount)
            self?.exchangeManager.setAmount(type.amount, flow: .direct)
        }
        view.set(currency: exchangeManager.sendCurrency.ticker, rate: nil, rateCurrency: nil)
        return view
    }()
    
    private lazy var getExchangeField: CurrencyExchangeField = {
        let view = CurrencyExchangeField(frame: .init(origin: .zero, size: .init(width: 200, height: 72)))
        view.set(title: ChangeNowSDK.localization.currencyExchangeFieldTitleGet())
        view.set(isEditable: false)
        view.set(currency: exchangeManager.getCurrency)
        view.set(fieldDelegate: self, chooseCurrencyAction: exchangeManager.baseSelectGetCurrencyAction)
        view.set(value: nil)
        return view
    }()
    
    private lazy var addressContainer: UIStackView = {
        let view = UIStackView(arrangedSubviews: [addressLabel, addressField, addressExtraIdField])
        view.axis = .vertical
        view.spacing = 12
        return view
    }()
    
    private lazy var addressLabel: UILabel = {
        let view = UILabel()
        view.font = .regular(ofSize: 16)
        view.textColor = .white
        view.text = ChangeNowSDK.localization.addressInputTitle()
        return view
    }()
    
    private lazy var addressField: AddressField = {
        let view = AddressField()
        view.placeholder = ChangeNowSDK.localization.addressInputAddressPlaceholder(tiker: exchangeManager.getCurrency.fullTiker)
        view.font = .light(ofSize: 14)
        view.textColor = .white
        return view
    }()
    
    private lazy var addressExtraIdField: AddressField = {
        let view = AddressField()
        view.placeholder = ChangeNowSDK.localization.addressInputAddressExtraIdPlaceholder()
        view.font = .light(ofSize: 14)
        view.textColor = .white
        view.isHidden = !exchangeManager.getCurrency.hasExternalId
        return view
    }()
    
    private(set) var exchangeManager: ExchangeManager
    private let showAddressInputs: Bool
    
    public init(frame: CGRect, exchangeManager: ExchangeManager, showAddressInputs: Bool = true) {
        self.exchangeManager = exchangeManager
        self.showAddressInputs = showAddressInputs
        super.init(frame: .zero)
        
        exchangeManager.delegate = self
        
        backgroundColor = .darkBG
        clipsToBounds = true
        setViews()
        setConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setViews() {
        addSubview(sendExchangeField)
        addSubview(rateView)
        addSubview(getExchangeField)
        if showAddressInputs {
            addSubview(addressContainer)
        }
    }
    
    private func setConstraints() {
        sendExchangeField.makeConstraints {
            [$0.topAnchor.constraint(equalTo: topAnchor, constant: 16),
             $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
             $0.bottomAnchor.constraint(equalTo: rateView.topAnchor, constant: -16),
             $0.heightAnchor.constraint(equalToConstant: 72)]
        }
        
        rateView.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
             $0.bottomAnchor.constraint(equalTo: getExchangeField.topAnchor, constant: -16),
             $0.heightAnchor.constraint(equalToConstant: 36)]
        }
        
        if showAddressInputs {
            getExchangeField.makeConstraints {
                [$0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
                 $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
                 $0.bottomAnchor.constraint(equalTo: addressContainer.topAnchor, constant: -40),
                 $0.heightAnchor.constraint(equalToConstant: 72)]
            }
            
            addressContainer.makeConstraints {
                [$0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
                 $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
                 $0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16)]
            }
            
            addressField.makeConstraints {
                [$0.heightAnchor.constraint(equalToConstant: 48)]
            }
            
            addressExtraIdField.makeConstraints {
                [$0.heightAnchor.constraint(equalToConstant: 48)]
            }
        } else {
            getExchangeField.makeConstraints {
                [$0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
                 $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
                 $0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16),
                 $0.heightAnchor.constraint(equalToConstant: 72)]
            }
        }
    }
    
    private let fromAmountScheduler = ActionScheduler(dispatch: .main)
    private func set(fromAmountString: String) {
        let fromAmountString = fromAmountString.isEmpty ? "0" : fromAmountString
        guard let fromAmount = Decimal(string: fromAmountString) else { return }
        exchangeManager.stopEstimating()
        fromAmountScheduler.invalidate()
        fromAmountScheduler.execute(after: 0.3) { [weak self] in
            self?.exchangeManager.setAmount(fromAmount, flow: .direct)
        }
    }
    
    private let toAmountScheduler = ActionScheduler(dispatch: .main)
    private func set(toAmountString: String) {
        let toAmountString = toAmountString.isEmpty ? "0" : toAmountString
        guard let toAmount = Decimal(string: toAmountString) else { return }
        exchangeManager.stopEstimating()
        toAmountScheduler.invalidate()
        toAmountScheduler.execute(after: 0.3) { [weak self] in
            self?.exchangeManager.setAmount(toAmount, flow: .reverse)
        }
    }
}

extension CNCalculatorView: UITextFieldDelegate {
    public func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        func onlyFloatDigits() {
            let positionOriginal = textField.beginningOfDocument
            let cursorLocation = textField.position(from: positionOriginal,
                                                    offset: (range.location + NSString(string: string).length))

            let filteredString = String(string
                .replacingOccurrences(of: ",", with: ".")
                .unicodeScalars
                .filter { CharacterSet.floatDigits.contains($0) }
            )
            if let text = textField.text,
                let textRange = Range(range, in: text) {
                var updatedText = text.replacingCharacters(in: textRange, with: filteredString)
                var components = updatedText.components(separatedBy: ".")
                if components.count == 2, components[1].count > 8 {
                    components[1] = String(components[1].prefix(8))
                    updatedText = components.joined(separator: ".")
                }
                textField.text = updatedText.floatDigit
            }
            if let cursorLoc = cursorLocation {
                textField.selectedTextRange = textField.textRange(from: cursorLoc, to: cursorLoc)
            }
        }

        if sendExchangeField.isSame(textField: textField) {
            onlyFloatDigits()
            set(fromAmountString: textField.text ?? "")
            return false
        } else if getExchangeField.isSame(textField: textField) {
            onlyFloatDigits()
            set(toAmountString: textField.text ?? "")
            return false
        }
        return false
    }
}

extension CNCalculatorView: ExchangeManagerDelegate {
    public var address: String? {
        return (addressField.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).nonEmpty
    }
    
    public var addressExtraId: String? {
        return (addressExtraIdField.text ?? "").trimmingCharacters(in: .whitespacesAndNewlines).nonEmpty
    }
    
    public func updateEstimateState(_ state: EstimateState, direction: CNExchangeType) {
        switch state {
        case .loading:
            switch direction {
            case .direct:
                getExchangeField.set(value: nil)
            case .reverse:
                sendExchangeField.set(value: nil)
            }
            rateView.setWarning(warningType: nil)
            rateView.set(currency: exchangeManager.sendCurrency.ticker, rate: nil, rateCurrency: nil)
        case let .success(fromAmount, toAmount):
            sendExchangeField.set(value: fromAmount)
            getExchangeField.set(value: toAmount)
            
            rateView.setWarning(warningType: nil)
            rateView.set(currency: exchangeManager.sendCurrency.ticker, rate: toAmount/fromAmount, rateCurrency: exchangeManager.getCurrency.ticker)
        case let .warning(type):
            switch direction {
            case .direct:
                sendExchangeField.setWarning()
                getExchangeField.set(value: nil)
            case .reverse:
                getExchangeField.setWarning()
                sendExchangeField.set(value: nil)
            }
            rateView.setWarning(warningType: type)
        }
    }
    
    public func didChangeSendCurrency() {
        sendExchangeField.set(currency: exchangeManager.sendCurrency)
    }
    
    public func didChangeGetCurrency() {
        addressExtraIdField.isHidden = !exchangeManager.getCurrency.hasExternalId
        addressField.placeholder = ChangeNowSDK.localization.addressInputAddressPlaceholder(tiker: exchangeManager.getCurrency.fullTiker)
        getExchangeField.set(currency: exchangeManager.getCurrency)
    }
    
    public func didChangeSelectSendCurrencyAction() {
        sendExchangeField.set(fieldDelegate: self, chooseCurrencyAction: exchangeManager.baseSelectSendCurrencyAction)
    }
    
    public func didChangeSelectGetCurrencyAction() {
        getExchangeField.set(fieldDelegate: self, chooseCurrencyAction: exchangeManager.baseSelectGetCurrencyAction)
    }
}
