//
//  CurrencyExchangeRateView.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 27.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

final class CurrencyExchangeRateView: UIView {
    typealias WarningType = EstimateState.WarningType

    // MARK: - Views

    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.textColor = .white
        view.font = .regular(ofSize: 14)
        return view
    }()

    private lazy var rateLable: EdgeInsetLabel = {
        let view = EdgeInsetLabel()
        view.textInsets = .init(top: 4, left: 4, bottom: 4, right: 4)
        view.font = .regular(ofSize: 14)
        view.textColor = .cnGreen
        view.backgroundColor = .mainBlack
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        return view
    }()

    private lazy var fieldActivityIndicator: NVActivityIndicatorView = {
        let view = NVActivityIndicatorView(frame: .zero, type: .ballBeat, color: .cnGreen, padding: nil)
        view.backgroundColor = .mainBlack
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        return view
    }()

    private lazy var warningView: WarningView = {
        let view = WarningView()
        view.isHidden = true
        view.addTarget(self, action: #selector(warningTap), for: .touchUpInside)
        return view
    }()
    
    var warninTapAction: ((WarningType) -> Void)?

    // MARK: - Life Cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubviews()
        setConstraints()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public

    func set(currency: String, rate: Decimal?, rateCurrency: String?) {
        titleLabel.text = "1 \(currency.uppercased()) ~"
        if let rate = rate,
           let rateCurrency = rateCurrency{
            rateLable.text = "\(rate.currencyValue) \(rateCurrency.uppercased())"
            fieldActivityIndicator.isHidden = true
            fieldActivityIndicator.stopAnimating()
            rateLable.isHidden = false
        } else {
            fieldActivityIndicator.isHidden = false
            fieldActivityIndicator.startAnimating()
            rateLable.isHidden = true
        }
        layoutIfNeeded()
    }

    private var lastWarningType: WarningType?
    func setWarning(warningType: WarningType?) {
        if let warningType = warningType {
            lastWarningType = warningType
            warningView.set(warning: warningType.warningText)
            warningView.isHidden = false
        } else {
            lastWarningType = nil
            warningView.isHidden = true
        }
    }

    // MARK: - Private

    private func addSubviews() {
        addSubview(titleLabel)
        addSubview(rateLable)
        addSubview(fieldActivityIndicator)
        addSubview(warningView)
    }

    private func setConstraints() {
        
        titleLabel.makeConstraints {
            [$0.centerYAnchor.constraint(equalTo: centerYAnchor),
             $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 12)]
        }
        
        rateLable.makeConstraints {
            [$0.centerYAnchor.constraint(equalTo: centerYAnchor),
             $0.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 4)]
        }
        
        fieldActivityIndicator.makeConstraints {
            [$0.centerYAnchor.constraint(equalTo: centerYAnchor),
             $0.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: 4),
             $0.widthAnchor.constraint(equalToConstant: 40),
             $0.heightAnchor.constraint(equalToConstant: 20)]
        }
        
        warningView.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: leadingAnchor),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor),
             $0.topAnchor.constraint(equalTo: topAnchor),
             $0.bottomAnchor.constraint(equalTo: bottomAnchor)]
        }
    }
    
    @objc private func warningTap() {
        if let lastWarningType = lastWarningType {
            warninTapAction?(lastWarningType)
        }
    }
}

private extension EstimateState.WarningType {
    var warningText: String {
        switch self {
        case let .maximum(value, tiker):
            return ChangeNowSDK.localization.maximumAmountWarning(amountAndTiker: "\(value.currencyValue) \(tiker.uppercased())")
        case let .minimum(value, tiker):
            return ChangeNowSDK.localization.minimumAmountWarning(amountAndTiker: "\(value.currencyValue) \(tiker.uppercased())")
        }
    }
}
