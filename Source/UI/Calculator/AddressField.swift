//
//  AddressField.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 01.05.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit

private enum Constants {
    static let placeholderAttr: [NSAttributedString.Key : Any] = {
        return [.font: UIFont.light(ofSize: 14), .foregroundColor: UIColor.medium2Gray]
    }()
}

final class AddressField: UITextField {
    
    private lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = .mediumGray
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(separator)
        separator.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: leadingAnchor),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor),
             $0.bottomAnchor.constraint(equalTo: bottomAnchor),
             $0.heightAnchor.constraint(equalToConstant: 0.99)]
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var placeholder: String? {
        didSet {
            attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: Constants.placeholderAttr)
        }
    }
}
