//
//  SelectCurrencyViewController.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit

public final class CNSelectCurrencyViewController: UIViewController {
    
    public enum PresentationType {
        case push
        case present(title: String)
    }
    
    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.font = .semiBold(ofSize: 20)
        view.textColor = .white
        if case .present(let title) = presentationType {
            view.text = title
        }
        return view
    }()
    
    private lazy var closeButton: UIButton = {
        let view = UIButton()
        view.setImage(UIImage.close, for: .normal)
        view.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        return view
    }()
    
    private lazy var searchBarView: SearchBarView = {
        let view = SearchBarView()
        view.layer.masksToBounds = true
        view.delegate = self
        return view
    }()
    
    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.backgroundColor = .clear
        view.keyboardDismissMode = .onDrag
        view.separatorStyle = .none
        view.delegate = self
        view.dataSource = self
        view.estimatedRowHeight = 56
        view.register(SelectCurrencyTableViewCell.self, forCellReuseIdentifier: SelectCurrencyTableViewCell.identifier)
        return view
    }()
    
    
    
    private let presentationType: PresentationType
    private let selectedCurrency: CurrencyType?
    private var currencies: [CurrencyType] = []
    private var searchableCurrencies: [CurrencyType] = []
    private let currenciesManager: CNCurrenciesManager
    
    private var lastSearchText: String = ""
    private var isSearchActive: Bool {
        !lastSearchText.isEmpty
    }
    
    public var selectCurrencyAction: ((CurrencyType) -> Void)?
    
    public init(currenciesManager: CNCurrenciesManager, selectedCurrency: CurrencyType?, presentationType: PresentationType) {
        self.presentationType = presentationType
        self.currenciesManager = currenciesManager
        self.selectedCurrency = selectedCurrency
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .darkBG
        
        setViews()
        setConstraints()
        
        currenciesManager.getCurrencies { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let currencies):
                    self?.currencies = currencies
                    self?.tableView.reloadData()
                case .failure(let error):
                    self?.showErrorIfNeeded(error: error)
                }
            }
        }
    }
    
    private func setViews() {
        view.addSubview(searchBarView)
        view.addSubview(tableView)
        
        if case .present = presentationType {
            view.addSubview(titleLabel)
            view.addSubview(closeButton)
        }
    }
    
    private func setConstraints() {
        if case .present = presentationType {
            titleLabel.makeConstraints {
                [$0.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                 $0.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16)]
            }
            closeButton.makeConstraints {
                [$0.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
                 $0.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -11)]
            }
            
            searchBarView.makeConstraints {
                [$0.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 24),
                 $0.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
                 $0.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
                 $0.heightAnchor.constraint(equalToConstant: 40)]
            }
        } else {
            searchBarView.makeConstraints {
                [$0.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
                 $0.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
                 $0.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
                 $0.heightAnchor.constraint(equalToConstant: 40)]
            }
        }
        
        tableView.makeConstraints {
            [$0.topAnchor.constraint(equalTo: searchBarView.bottomAnchor, constant: 16),
             $0.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
             $0.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
             $0.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16)]
        }
    }
    
    private func showErrorIfNeeded(error: CNNetworkError) {
        let errorMessage: String?
        switch error {
        case .api(let apiError):
            errorMessage = apiError.message ?? "Unown"
        case .underlying(let underlyingError), .jsonDecoder(let underlyingError):
            errorMessage = underlyingError.localizedDescription
        case .unown, .emptyResult:
            errorMessage = nil
        }
        
        guard let errorMessage = errorMessage else { return }
        
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func updateSearchData(searchText: String) {
        let searchText = searchText.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()
        
        if searchText.isEmpty {
            searchableCurrencies = []
        } else {
            searchableCurrencies = currencies.filter {
                $0.ticker.lowercased().contains(searchText) || $0.name.lowercased().contains(searchText)
            }
        }
        lastSearchText = searchText
        
        tableView.reloadData()
    }
    
    @objc private func closeAction() {
        self.dismiss(animated: true)
    }
}

extension CNSelectCurrencyViewController: UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive {
            return searchableCurrencies.count
        } else {
            return currencies.count
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currency: CurrencyType?
        if isSearchActive {
            currency = searchableCurrencies[safe: indexPath.row]
        } else {
            currency = currencies[safe: indexPath.row]
        }
        
        guard let currency = currency,
              let cell = tableView.dequeueReusableCell(withIdentifier: SelectCurrencyTableViewCell.identifier, for: indexPath) as? SelectCurrencyTableViewCell else { return UITableViewCell() }
        
        cell.set(currency: currency)
        
        if let selectedCurrency = selectedCurrency,
           currency.ticker == selectedCurrency.ticker,
           currency.network == selectedCurrency.network {
            cell.isSelected = true
        }
        
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currency: CurrencyType?
        if isSearchActive {
            currency = searchableCurrencies[safe: indexPath.row]
        } else {
            currency = currencies[safe: indexPath.row]
        }
        
        if let currency = currency {
            selectCurrencyAction?(currency)
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
}

extension CNSelectCurrencyViewController: SearchBarViewDelegate {

    func searchBarReturnButtonPressed() { }

    func searchBarDidChangeText(searchText: String) {
        updateSearchData(searchText: searchText)
        tableView.reloadData()
    }
}

