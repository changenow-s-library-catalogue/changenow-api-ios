//
//  SelectCurrencyTableViewCell.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit

final class SelectCurrencyTableViewCell: UITableViewCell {
    
    static let identifier = "SelectCurrencyTableViewCell"
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    // MARK: - Views

    private lazy var selectionView: UIView = {
        let view = UIView()
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 12
        return view
    }()

    private lazy var currencyImageView = UIImageView()
    
    private lazy var nameStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [tikerAndNetworkStackView, nameLabel])
        view.spacing = 1
        view.alignment = .leading
        view.axis = .vertical
        return view
    }()
    
    private lazy var tikerAndNetworkStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [tickerLabel, networkLabel])
        view.axis = .horizontal
        view.spacing = 6
        view.alignment = .center
        return view
    }()

    private lazy var tickerLabel: UILabel = {
        let view = UILabel()
        view.textColor = .white
        view.font = .medium(ofSize: 18)
        return view
    }()

    private lazy var networkLabel: UILabel = {
        let view = EdgeInsetLabel()
        view.textInsets = .init(top: 1, left: 4, bottom: 1, right: 4)
        view.textColor = .lightGrey
        view.font = .medium(ofSize: 8)
        view.textAlignment = .center
        view.layer.cornerRadius = 4
        view.layer.masksToBounds = true
        return view
    }()

    private lazy var nameLabel: UILabel = {
        let view = UILabel()
        view.textColor = .medium2Gray
        view.font = .regular(ofSize: 12)
        return view
    }()

    private lazy var separator: UIView = {
        let view = UIView()
        view.backgroundColor = .darkPurple
        return view
    }()
    
    private weak var currencyCancalable: Cancalable?

    // MARK: - Life Cycle

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    private func commonInit() {
        addSubviews()
        setConstraints()

        backgroundColor = .clear
        contentView.backgroundColor = .clear
        currencyImageView.image = .currencyPlaceholder
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        currencyCancalable?.cancel()
        currencyImageView.image = .currencyPlaceholder
    }

    // MARK: - Public

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        if isSelected || highlighted {
            selectionView.backgroundColor = .darkPurple
            separator.isHidden = true
        } else {
            selectionView.backgroundColor = .clear
            separator.isHidden = false
        }
    }

    func set(currency: CurrencyType) {
        tickerLabel.text = currency.tickerForShow
        nameLabel.text = currency.name
        
        if let networkType = currency.networkType {
            networkLabel.isHidden = false
            networkLabel.text = networkType.networkForShow
            networkLabel.backgroundColor = networkType.color
        } else {
            networkLabel.isHidden = true
        }
        
        currencyCancalable = currency.getIcon { [weak currencyImageView] icon in
            if let icon = icon {
                DispatchQueue.main.async {
                    currencyImageView?.image = icon
                }
            }
        }
        
        updateSelection()
    }

    // MARK: - Private

    private func updateSelection() {
        if isSelected {
            selectionView.backgroundColor = .darkPurple
            separator.isHidden = true
        } else {
            selectionView.backgroundColor = .clear
            separator.isHidden = false
        }
    }

    private func addSubviews() {
        contentView.addSubview(containerView)
        containerView.addSubview(selectionView)
        containerView.addSubview(currencyImageView)
        containerView.addSubview(nameStackView)
        containerView.addSubview(separator)
    }

    private func setConstraints() {
        containerView.makeConstraints {
            [$0.topAnchor.constraint(equalTo: contentView.topAnchor),
             $0.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
             $0.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
             $0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16)]
        }
        
        selectionView.makeConstraints {
            [$0.topAnchor.constraint(equalTo: containerView.topAnchor),
             $0.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
             $0.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
             $0.trailingAnchor.constraint(equalTo: containerView.trailingAnchor)]
        }
        
        currencyImageView.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 16),
             $0.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
             $0.widthAnchor.constraint(equalToConstant: 24),
             $0.heightAnchor.constraint(equalToConstant: 24)]
        }
        
        nameStackView.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: currencyImageView.trailingAnchor, constant: 12),
             $0.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)]
        }
        
        separator.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: nameStackView.leadingAnchor),
             $0.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
             $0.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
             $0.heightAnchor.constraint(equalToConstant: 0.99)]
        }
    }
}

fileprivate enum NetworkType {
    enum DefaultNetwork: String {
        case erc20 = "eth"
        case trc20 = "trx"
        case bep2 = "bnb"
        case bep20 = "bsc"
    }

    case `default`(DefaultNetwork)
    case unknown(String)

    init(network: String) {
        if let defaultNetwork = DefaultNetwork(rawValue: network.lowercased()) {
            self = .default(defaultNetwork)
        } else {
            self = .unknown(network)
        }
    }

    var networkForShow: String {
        switch self {
        case let .default(network):
            return network.rawValue.uppercased()
        case let .unknown(network):
            return network.uppercased()
        }
    }

    var color: UIColor {
        switch self {
        case let .default(network):
            switch network {
            case .erc20:
                return UIColor(red: 0.314, green: 0.686, blue: 0.584, alpha: 0.2)
            case .trc20:
                return UIColor(red: 0.922, green: 0.196, blue: 0.165, alpha: 0.2)
            case .bep2, .bep20:
                return UIColor(red: 1, green: 0.765, blue: 0.192, alpha: 0.2)
            }
        case .unknown:
            return UIColor.white.withAlphaComponent(0.2)
        }

    }
}

fileprivate extension CurrencyType {
    var networkType: NetworkType? {
        if let network = networkForShow {
            return NetworkType(network: network)
        } else {
            return nil
        }
    }
}
