//
//  UIModuleExport.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

#if SWIFT_PACKAGE
@_exported import Base
@_exported import ChangeNow_SDK_API
#endif
