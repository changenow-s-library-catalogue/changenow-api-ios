//
//  CurrenciesManager.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 27.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public final class CNCurrenciesManager {
    
    private let apiService: CNApiService
    private let config: Configuration
    
    private lazy var cachedCurrencies: [CNCurrency] = {
        return currenciesStorageModel?.currencies ?? []
    }() {
        didSet {
            currenciesStorageModel = CurrenciesStorageModel(currencies: cachedCurrencies, updateDate: Date())
        }
    }
    private lazy var currenciesStorageModel: CurrenciesStorageModel? = {
        let storage: CurrenciesStorageModel? = try? FileStorage.content(from: .cache, filename: .init(name: "CNCurrenciesStorage", fileExtension: .plist))
        return storage
    }() {
        didSet {
            _ = try? FileStorage.store(objects: currenciesStorageModel, to: .cache, as: .init(name: "CNCurrenciesStorage", fileExtension: .plist))
        }
    }
    
    public init(apiService: CNApiService, config: Configuration = .init()) {
        self.apiService = apiService
        self.config = config
        
        if config.updateCacheOnInit {
            updateCache()
        }
    }
    
    public func getCurrencies(completion: @escaping ((Result<[CNCurrency], CNNetworkError>) -> Void)) {
        
        if config.cacheLifetime.secons <= 0 {
            fetchCurrencies { [weak self] result in
                guard let self = self else {
                    completion(.failure(.unown))
                    return
                }
                
                completion(self.filterFetchCurrenciesResult(result))
            }
            return
        }
        
        guard let updateDate = currenciesStorageModel?.updateDate else {
            fetchCurrencies { [weak self] result in
                guard let self = self else {
                    completion(.failure(.unown))
                    return
                }
                
                completion(self.filterFetchCurrenciesResult(result))
            }
            updateCache()
            return
        }
        
        if cachedCurrencies.count > 0 {
            completion(.success(filterСurrenciesIfNeeded(cachedCurrencies)))
        } else {
            fetchCurrencies { [weak self] result in
                guard let self = self else {
                    completion(.failure(.unown))
                    return
                }
                completion(self.filterFetchCurrenciesResult(result))
                if case .success(let currencies) = result {
                    self.cachedCurrencies = currencies
                }
            }
        }
        
        if seconds(from: updateDate, to: Date()) > config.cacheLifetime.secons {
            updateCache()
        }
    }
    
    private var isCacheUpdating: Bool = false
    private func updateCache() {
        guard !isCacheUpdating else { return }
        isCacheUpdating = true
        
        fetchCurrencies { [weak self] result in
            self?.isCacheUpdating = false
            if case .success(let currencies) = result {
                self?.cachedCurrencies = currencies
            }
        }
    }
    
    private func fetchCurrencies(completion: @escaping ((Result<[CNCurrency], CNNetworkError>) -> Void)) {
        apiService.fetchAvailableCurrencies(onlyActive: true, flow: config.flow, withBuy: config.withBuy, withSell: config.withSell, completion: completion)
    }
    
    private func filterFetchCurrenciesResult(_ result: Result<[CNCurrency], CNNetworkError>) -> Result<[CNCurrency], CNNetworkError> {
        switch result {
        case .success(let currencies):
            return .success(self.filterСurrenciesIfNeeded(currencies))
        case .failure(let error):
            return .failure(error)
        }
    }
    
    private func seconds(from fromDate: Date, to toDate: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: fromDate, to: toDate).second ?? 0
    }
    
    private func filterСurrenciesIfNeeded(_ currencies: [CNCurrency]) -> [CNCurrency] {
        guard let filter = config.filter else {
            return currencies
        }
        
        var filteredCurrencies = currencies
        switch filter {
        case .blackList(let items):
            for item in items {
                filteredCurrencies = filteredCurrencies.filter { !item.isFit(currency: $0) }
            }
        case .whiteList(let items):
            for item in items {
                filteredCurrencies = filteredCurrencies.filter { item.isFit(currency: $0) }
            }
        }
        
        return filteredCurrencies
    }
}

public extension CNCurrenciesManager {
    struct Configuration {
        let updateCacheOnInit: Bool
        let cacheLifetime: Time
        let flow: CNExchangeFlow
        let withBuy: Bool?
        let withSell: Bool?
        let filter: Filter?
        
        public init(updateCacheOnInit: Bool = true, cacheTime: CNCurrenciesManager.Configuration.Time = .seconds(60), flow: CNExchangeFlow = .standard, withBuy: Bool? = nil, withSell: Bool? = nil, filter: Filter? = nil) {
            self.updateCacheOnInit = updateCacheOnInit
            self.cacheLifetime = cacheTime
            self.flow = flow
            self.withBuy = withBuy
            self.withSell = withSell
            self.filter = filter
        }
        
        
        public enum Time {
            case seconds(Int)
            case minutes(Int)
            case hours(Int)
            
            var secons: Int {
                switch self {
                case .seconds(let value):
                    return value
                case .minutes(let value):
                    return value * 60
                case .hours(let value):
                    return value * 60 * 60
                }
            }
        }
        
        public enum Filter {
            case whiteList([Item])
            case blackList([Item])
            
            public enum Item {
                case tiker(String)
                case network(String)
                case tikerAndNetwork(tiker: String, network: String)
                
                func isFit(currency: CNCurrency) -> Bool {
                    switch self {
                    case let .tiker(tiker):
                        return currency.ticker.lowercased() == tiker.lowercased()
                    case let .network(network):
                        return currency.network.lowercased() == network.lowercased()
                    case let .tikerAndNetwork(tiker, network):
                        return currency.ticker.lowercased() == tiker.lowercased() && currency.network.lowercased() == network.lowercased()
                    }
                }
            }
        }
    }
    
    struct CurrenciesStorageModel: Codable {
        let currencies: [CNCurrency]
        let updateDate: Date
    }
}
