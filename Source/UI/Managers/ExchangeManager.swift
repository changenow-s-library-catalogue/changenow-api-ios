//
//  ExchangeManager.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 27.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation
import UIKit

public protocol ExchangeManagerDelegate: AnyObject {
    var address: String? { get }
    var addressExtraId: String? { get }
    
    func didChangeSendCurrency()
    func didChangeGetCurrency()
    
    func didChangeSelectSendCurrencyAction()
    func didChangeSelectGetCurrencyAction()
    
    func updateEstimateState(_ state: EstimateState, direction: CNExchangeType)
}

public protocol ExchangeManager: AnyObject {
    var sendCurrency: CurrencyType { get }
    var getCurrency: CurrencyType { get }
    
    var amount: Decimal { get }
    
    var delegate: ExchangeManagerDelegate? { get set }
    
    var selectSendCurrencyAction: ((CurrencyType) -> Void)? { get }
    var selectGetCurrencyAction: ((CurrencyType) -> Void)? { get }
    
    func stopEstimating()
    func setAmount(_ amount: Decimal, flow: CNExchangeType)
}

extension ExchangeManager {
    var baseSelectSendCurrencyAction: (() -> Void)? {
        if let selectSendCurrencyAction = selectSendCurrencyAction {
            return { [weak self] in
                guard let self = self else { return }
                selectSendCurrencyAction(self.sendCurrency)
            }
        } else {
            return nil
        }
    }
    
    var baseSelectGetCurrencyAction: (() -> Void)? {
        if let selectGetCurrencyAction = selectGetCurrencyAction {
            return { [weak self] in
                guard let self = self else { return }
                selectGetCurrencyAction(self.getCurrency)
            }
        } else {
            return nil
        }
    }
}

public enum EstimateState {
    case loading
    case warning(type: WarningType)
    case success(fromAmount: Decimal, toAmount: Decimal)
    
    public enum WarningType: Equatable {
        case minimum(amount: Decimal, tiker: String)
        case maximum(amount: Decimal, tiker: String)
        
        var amount: Decimal {
            switch self {
            case .minimum(let amount, _):
                return amount
            case .maximum(let amount, _):
                return amount
            }
        }
    }
}

public final class CNExchangeDefaultManager: ExchangeManager {
    
    public enum ErrorPlace {
        case estimate
        case exchangeRange
    }
    
    public var sendCurrency: CurrencyType {
        didSet {
            if oldValue.ticker != sendCurrency.ticker || oldValue.network != sendCurrency.network {
                delegate?.didChangeSendCurrency()
                updateExchangeRange()
            }
        }
    }
    public var getCurrency: CurrencyType {
        didSet {
            if oldValue.ticker != getCurrency.ticker || oldValue.network != getCurrency.network {
                delegate?.didChangeGetCurrency()
                updateExchangeRange()
            }
        }
    }
    
    public var selectSendCurrencyAction: ((CurrencyType) -> Void)? {
        didSet {
            delegate?.didChangeSelectSendCurrencyAction()
        }
    }
    public var selectGetCurrencyAction: ((CurrencyType) -> Void)? {
        didSet {
            delegate?.didChangeSelectGetCurrencyAction()
        }
    }
    
    public weak var delegate: ExchangeManagerDelegate?
    public var errorHandler: ((ErrorPlace, CNNetworkError) -> Void)?
    
    private let apiService: CNApiService
    
    public var amount: Decimal
    private var exchangeRange: CNExchangeRange? {
        didSet {
            if exchangeRange != nil {
                requestEstimate()
            } else {
                delegate?.updateEstimateState(.loading, direction: .direct)
            }
        }
    }
    
    private var lastEstimatedExchange: CNEstimatedExchange?
    private weak var estimatingUpdatingDataTask: URLSessionDataTask?
    
    public init(configuration: Configuration) {
        sendCurrency = configuration.initSendCurrency
        getCurrency = configuration.initGetCurrency
        amount = configuration.initAmount
        apiService = configuration.apiService
        
        updateExchangeRange()
    }
    
    public func stopEstimating() {
        estimatingUpdatingDataTask?.cancel()
    }
    
    public func setAmount(_ amount: Decimal, flow: CNExchangeType) {
        self.amount = amount
        requestEstimate()
    }
    
    public func getExchangePrepareData() -> CNExchangePrepareData? {
        guard let fromAmount = lastEstimatedExchange?.fromAmount,
              let toAmount = lastEstimatedExchange?.toAmount else { return nil }
        
        return .init(fromTicker: sendCurrency.ticker,
                     fromNetwork: sendCurrency.network,
                     fromAmount: fromAmount,
                     estimatedToAmount: toAmount,
                     toTicker: getCurrency.ticker,
                     toNetwork: getCurrency.network,
                     address: delegate?.address,
                     addressExtraId: delegate?.addressExtraId)
    }
    
    private func requestEstimate() {
        lastEstimatedExchange = nil
        if let exchangeRange = exchangeRange {
            if exchangeRange.minAmount > amount {
                delegate?.updateEstimateState(.warning(type: .minimum(amount: exchangeRange.minAmount, tiker: sendCurrency.ticker)), direction: .direct)
                return
            } else if let maxAmount = exchangeRange.maxAmount,
                      maxAmount < amount{
                delegate?.updateEstimateState(.warning(type: .maximum(amount: maxAmount, tiker: sendCurrency.ticker)), direction: .direct)
                return
            }
        }
        
        delegate?.updateEstimateState(.loading, direction: .direct)
        guard amount > 0 else {
            return
        }
        
        estimatingUpdatingDataTask = apiService.fetchEstimatedExchangeAmount(fromCurrency: sendCurrency.ticker,
                                                                             toCurrency: getCurrency.ticker,
                                                                             fromNetwork: sendCurrency.network,
                                                                             toNetwork: getCurrency.network,
                                                                             flow: .standard,
                                                                             type: .direct,
                                                                             amount: amount,
                                                                             useRateId: false) { [weak self] result in
            guard let self = self else { return }
            DispatchQueue.main.sync {
                switch result {
                case .success(let estimatedExchange):
                    self.delegate?.updateEstimateState(.success(fromAmount: estimatedExchange.fromAmount, toAmount: estimatedExchange.toAmount), direction: .direct)
                    self.lastEstimatedExchange = estimatedExchange
                case .failure(let error):
                    self.errorHandler?(.estimate, error)
                }
            }
        }
    }
    
    private weak var exchangeRangeUpdatingDataTask: URLSessionDataTask?
    private func updateExchangeRange() {
        lastEstimatedExchange = nil
        exchangeRange = nil
        exchangeRangeUpdatingDataTask?.cancel()
        exchangeRangeUpdatingDataTask = apiService.fetchExchangeRange(fromCurrency: sendCurrency.ticker,
                                                                      toCurrency: getCurrency.ticker,
                                                                      fromNetwork: sendCurrency.network,
                                                                      toNetwork: getCurrency.network,
                                                                      flow: .standard) { [weak self] result in
            DispatchQueue.main.sync {
                switch result {
                case .success(let exchangeRange):
                    self?.exchangeRange = exchangeRange
                case .failure(let error):
                    self?.errorHandler?(.exchangeRange, error)
                }
            }
        }
    }
}

public extension CNExchangeDefaultManager {
    struct Configuration {
        
        let apiService: CNApiService
        let initSendCurrency: CurrencyType
        let initGetCurrency: CurrencyType
        let initAmount: Decimal
        
        public init(apiService: CNApiService,
                    initSendCurrency: CurrencyType = DefaultCurrency.btc,
                    initGetCurrency: CurrencyType = DefaultCurrency.eth,
                    initAmount: Decimal = 1) {
            self.apiService = apiService
            self.initSendCurrency = initSendCurrency
            self.initGetCurrency = initGetCurrency
            self.initAmount = initAmount
        }
        
        public enum DefaultCurrency: CurrencyType {
            case btc
            case eth
            
            public var hasExternalId: Bool {
                return false
            }
            
            public var ticker: String {
                switch self {
                case .btc:
                    return "btc"
                case .eth:
                    return "eth"
                }
            }
            
            public var network: String {
                switch self {
                case .btc:
                    return "btc"
                case .eth:
                    return "eth"
                }
            }
            
            public var name: String {
                switch self {
                case .btc:
                    return "Bitcoin"
                case .eth:
                    return "Ethereum"
                }
            }
            
            public func getIcon(_ completion: @escaping ((UIImage?) -> Void)) -> Cancalable? {
                switch self {
                case .btc:
                    completion(.btcIcon)
                case .eth:
                    completion(.ethIcon)
                }
                return nil
            }
        }
    }
}
