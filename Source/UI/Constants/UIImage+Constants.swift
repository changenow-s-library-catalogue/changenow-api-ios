//
//  UIImage+Constants.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 26.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit
import Foundation

#if SWIFT_PACKAGE
private let mainBundle = Bundle.module
#else
private let mainBundle = Bundle(for: ChangeNowSDK.self)
#endif

extension UIImage {
    static let carretDown = UIImage(named: "carretDown", in: mainBundle, compatibleWith: nil)
    static let btcIcon = UIImage(named: "btc", in: mainBundle, compatibleWith: nil)
    static let ethIcon = UIImage(named: "eth", in: mainBundle, compatibleWith: nil)
    static let currencyPlaceholder = UIImage(named: "currencyPlaceholder", in: mainBundle, compatibleWith: nil)
    static let search = UIImage(named: "search", in: mainBundle, compatibleWith: nil)
    static let close = UIImage(named: "close", in: mainBundle, compatibleWith: nil)
    static let searchClear = UIImage(named: "searchClear", in: mainBundle, compatibleWith: nil)
}
