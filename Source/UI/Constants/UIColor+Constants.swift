//
//  UIColor+Constants.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 26.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit

extension UIColor {
    ///#00C26F
    static let cnGreen = UIColor(red: 0, green: 0.761, blue: 0.435, alpha: 1)
    ///#35354C
    static let darkBG = UIColor(red: 0.208, green: 0.208, blue: 0.298, alpha: 1)
    ///#49496C
    static let mediumPurple = UIColor(red: 0.286, green: 0.286, blue: 0.424, alpha: 1)
    ///#3D3D58
    static let darkPurple = UIColor(red: 0.239, green: 0.239, blue: 0.345, alpha: 1)
    ///#272740
    static let mainBlack = UIColor(red: 0.153, green: 0.153, blue: 0.251, alpha: 1)
    ///#9C9CA7
    static let medium2Gray = UIColor(red: 0.612, green: 0.612, blue: 0.655, alpha: 1)
    ///#9696A5
    static let mediumGray = UIColor(red: 0.588, green: 0.588, blue: 0.647, alpha: 1)
    ///#D3D7DA
    static let lightGrey = UIColor(red: 0.827, green: 0.843, blue: 0.855, alpha: 1)
    ///#DABE66
    static let alertStock = UIColor(red: 0.854, green: 0.746, blue: 0.399, alpha: 1)
    ///#4D4743
    static let alertDarkStock = UIColor(red: 0.302, green: 0.278, blue: 0.263, alpha: 1)
}
