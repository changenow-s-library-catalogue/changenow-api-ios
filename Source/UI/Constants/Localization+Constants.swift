//
//  Localization+Constants.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 05.05.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

extension ChangeNowSDK.Localization {
    func maximumAmountWarning(amountAndTiker: String) -> String {
        let format = Bundle.localizedString(forKey: "MaximumAmountWarning", andForceLocalization: forceLanguage)
        return String(format: format, locale: Locale.current, amountAndTiker)
    }
    
    func minimumAmountWarning(amountAndTiker: String) -> String {
        let format = Bundle.localizedString(forKey: "MinimumAmountWarning", andForceLocalization: forceLanguage)
        return String(format: format, locale: Locale.current, amountAndTiker)
    }
    
    func currencyExchangeFieldTitleSend() -> String {
        return Bundle.localizedString(forKey: "CurrencyExchangeField.Title.Send", andForceLocalization: forceLanguage)
    }
    
    func currencyExchangeFieldTitleGet() -> String {
        return Bundle.localizedString(forKey: "CurrencyExchangeField.Title.Get", andForceLocalization: forceLanguage)
    }
    
    func selectCurrencySearchBarViewPlaceholder() -> String {
        return Bundle.localizedString(forKey: "SelectCurrency.SearchBarView.Placeholder", andForceLocalization: forceLanguage)
    }
    
    func addressInputTitle() -> String {
        return Bundle.localizedString(forKey: "AddressInput.Title", andForceLocalization: forceLanguage)
    }
    
    func addressInputAddressPlaceholder(tiker: String) -> String {
        let format = Bundle.localizedString(forKey: "AddressInput.Address.Placeholder", andForceLocalization: forceLanguage)
        return String(format: format, locale: Locale.current, tiker)
    }
    
    func addressInputAddressExtraIdPlaceholder() -> String {
        return Bundle.localizedString(forKey: "AddressInput.AddressExtraId.Placeholder", andForceLocalization: forceLanguage)
    }
}
