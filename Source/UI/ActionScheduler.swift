//
//  ActionScheduler.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 30.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

final class ActionScheduler {

    // MARK: - Properties

    private let dispatch: DispatchQueue
    private var currentWorkItem: DispatchWorkItem?
    private var action: (() -> Void)?

    private var monitor = os_unfair_lock_s()

    // MARK: - Public

    init(dispatch: DispatchQueue = .global(qos: .default)) {
        self.dispatch = dispatch
    }

    func execute(after interval: TimeInterval, action: @escaping (() -> Void)) {
        os_unfair_lock_lock(&monitor)
        invalidate()
        self.action = action
        weak var item: DispatchWorkItem?
        currentWorkItem = DispatchWorkItem { [weak self] in
            if item?.isCancelled == false {
                self?.action?()
            }
        }
        item = currentWorkItem
        dispatch.asyncAfter(deadline: .now() + interval, execute: currentWorkItem!)
        os_unfair_lock_unlock(&monitor)
    }

    func invalidate() {
        currentWorkItem?.cancel()
        currentWorkItem = nil
        action = nil
    }
}
