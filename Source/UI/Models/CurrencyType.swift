//
//  CurrencyType.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 26.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit
import SVGKit

public protocol Cancalable: AnyObject {
    func cancel()
    var isCancelled: Bool { get }
}

public protocol CurrencyType {
    var ticker: String { get }
    var network: String { get }
    var name: String { get }
    var hasExternalId: Bool { get }
    
    func getIcon(_ completion: @escaping ((UIImage?) -> Void)) -> Cancalable?
}

extension CurrencyType {
    var tickerForShow: String {
        return ticker.uppercased()
    }
    var networkForShow: String? {
        if ticker.lowercased() != network.lowercased() {
            return network.uppercased()
        }
        return nil
    }
    var fullTiker: String {
        if let networkForShow = networkForShow {
            return "\(tickerForShow) (\(networkForShow.lowercased()))"
        } else {
            return tickerForShow
        }
    }
}

extension CNCurrency: CurrencyType {
    
    public func getIcon(_ completion: @escaping ((UIImage?) -> Void)) -> Cancalable? {
        let operation = CVGOperation(url: self.image)
        operation.completionBlock = { [weak operation] in
            if !(operation?.isCancelled ?? true),
               let image = operation?.outputImage {
                completion(image)
            } else {
                completion(nil)
            }
        }
        svgParserQueue.addOperation(operation)
        return operation
    }
    
    private var svgParserQueue: OperationQueue {
        guard let queue = objc_getAssociatedObject(self, &AssociatedKeys.svgParserQueue) as? OperationQueue else {
            let queue = OperationQueue()
            queue.maxConcurrentOperationCount = 10
            queue.qualityOfService = .default
            objc_setAssociatedObject(self, &AssociatedKeys.svgParserQueue, queue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            return queue
        }
        return queue
    }
}

private class CVGOperation: Operation, Cancalable {
    var outputImage: UIImage?
    private var url: String
    
    init(url: String) {
        self.url = url
    }
    
    override func main() {
        if !isCancelled {
            let source = SVGKSourceURL.source(from: URL(string: url))
            let svgImage = SVGKImage(source: source)
            svgImage?.size = .init(width: 24, height: 24)
            outputImage = svgImage?.uiImage
        }
    }
}

private struct AssociatedKeys {
    static var svgParserQueue = "svgParserQueue"
}
