//
//  ExchangePrepareData.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 01.05.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

public struct CNExchangePrepareData {
    public let fromTicker: String
    public let fromNetwork: String?
    public let fromAmount: Decimal
    public let estimatedToAmount: Decimal
    public let toTicker: String
    public let toNetwork: String?
    
    public let address: String?
    public let addressExtraId: String?
}
