//
//  SearchBarView.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit

protocol SearchBarViewDelegate: AnyObject {

    func searchBarReturnButtonPressed()
    func searchBarDidChangeText(searchText: String)
}

extension SearchBarViewDelegate {

    func searchBarReturnButtonPressed() { }
}

final class SearchBarView: UIView, UITextFieldDelegate {

    // MARK: - Properties

    weak var delegate: SearchBarViewDelegate?

    var text: String {
        get {
            return textField.text ?? ""
        }
        set {
            textField.text = newValue
        }
    }

    // MARK: - Private

    private lazy var searchIcon: UIImageView = {
        let searchIcon = UIImageView()
        searchIcon.image = .search
        return searchIcon
    }()

    private lazy var textField: UITextField = {
        let textField = UITextField()
        textField.font = .regular(ofSize: 14)
        textField.textColor = .white
        textField.backgroundColor = .clear
        let paragraphStyle = NSMutableParagraphStyle()
        textField.attributedPlaceholder = NSAttributedString(
            string: placeholder,
            attributes: [.font: UIFont.regular(ofSize: 14),
                         .foregroundColor: UIColor.medium2Gray,
                         .paragraphStyle: paragraphStyle]
        )
        textField.clearButtonMode = .never
        textField.returnKeyType = .done
        textField.autocorrectionType = .no
        textField.spellCheckingType = .no
        textField.delegate = self
        return textField
    }()

    private lazy var clearButton: UIButton = {
        let clearButton = UIButton()
        clearButton.setImage(UIImage.searchClear, for: .normal)
        clearButton.addTarget(self, action: #selector(clearButtonAction), for: .touchUpInside)
        clearButton.imageView?.tintColor = .mediumGray
        clearButton.isHidden = true
        return clearButton
    }()

    private let placeholder: String = ChangeNowSDK.localization.selectCurrencySearchBarViewPlaceholder()

    // MARK: - Life Cycle

    init() {
        super.init(frame: .zero)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }

    private func commonInit() {
        addSubviews()
        makeConstraints()

        backgroundColor = .mainBlack
        layer.masksToBounds = true
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        layer.cornerRadius = bounds.height / 2
    }

    override func becomeFirstResponder() -> Bool {
        return textField.becomeFirstResponder()
    }

    override var isFirstResponder: Bool {
        return textField.isFirstResponder
    }

    func reset() {
        textField.text = ""
        clearButton.isHidden = true
    }

    // MARK: - Private

    private func addSubviews() {
        addSubview(searchIcon)
        addSubview(textField)
        addSubview(clearButton)
    }

    private func makeConstraints() {
        searchIcon.makeConstraints {
            [$0.centerYAnchor.constraint(equalTo: centerYAnchor),
             $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
             $0.widthAnchor.constraint(equalToConstant: 16),
             $0.heightAnchor.constraint(equalToConstant: 16)]
        }
        
        textField.makeConstraints {
            [$0.leadingAnchor.constraint(equalTo: searchIcon.trailingAnchor, constant: 8),
             $0.trailingAnchor.constraint(equalTo: clearButton.leadingAnchor),
             $0.centerYAnchor.constraint(equalTo: centerYAnchor)]
        }
        
        clearButton.makeConstraints {
            [$0.centerYAnchor.constraint(equalTo: centerYAnchor),
             $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15),
             $0.widthAnchor.constraint(equalToConstant: 12),
             $0.heightAnchor.constraint(equalToConstant: 12)]
        }
    }

    // MARK: - UITextFieldDelegate

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            textField.text = updatedText
            delegate?.searchBarDidChangeText(searchText: updatedText)
            clearButton.isHidden = updatedText.isEmpty
        } else {
            clearButton.isHidden = true
        }
        return false
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        delegate?.searchBarReturnButtonPressed()
        textField.resignFirstResponder()
        return true
    }
    
    @objc func clearButtonAction() {
        text = ""
        clearButton.isHidden = true
        delegate?.searchBarDidChangeText(searchText: "")
    }
}

