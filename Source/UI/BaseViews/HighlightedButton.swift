//
//  HighlightedButton.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 26.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit

class HighlightedButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            if newValue {
                self.alpha = 0.5
            } else {
                self.alpha = 1
            }
            super.isHighlighted = newValue
        }
    }

    private func commonInit() {
        adjustsImageWhenHighlighted = false
        adjustsImageWhenDisabled = false
    }
}

