//
//  Bundle+Extensions.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 27.03.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

extension Bundle {
    
    struct Constants {
        static let bundleNeme = "ChangeNowUISDK"
        static let bundleExtension = "bundle"
        static let englishLocalization = "en"
        static let projectExtension = "lproj"
        static let table = "Localizable"
    }

    final class func localizedString(forKey key: String, andForceLocalization forcedLanguage: ChangeNowSDK.Localization.Language?) -> String {
        guard var path = cnBundlePath() else {
            return key
        }

        if let deviceLangauge = deviceLanguage(),
            let devicePath = cnForcedBundlePath(forceLanguageLocalization: deviceLangauge) {
            path = devicePath
        }

        if let forcedLanguage = forcedLanguage,
            let forcedPath = cnForcedBundlePath(forceLanguageLocalization: forcedLanguage) {
            path = forcedPath
        }

        return Bundle(path: path)?.localizedString(forKey: key, value: key, table: Constants.table) ?? key
    }
}

extension Bundle {
    
    final class func cnBundlePath() -> String? {
        #if SWIFT_PACKAGE
        return Bundle.module.path(forResource: Constants.bundleNeme, ofType: Constants.bundleExtension)
        #else
        return Bundle(for: ChangeNowSDK.self).path(forResource: Constants.bundleNeme, ofType: Constants.bundleExtension)
        #endif
    }

    final class func cnForcedBundlePath(forceLanguageLocalization: ChangeNowSDK.Localization.Language) -> String? {
        guard let path = cnBundlePath() else { return nil }
        let name = forceLanguageLocalization.rawValue

        return Bundle(path: path)?.path(forResource: name, ofType: Constants.projectExtension)
    }

    /// The user's preferred language based on their device's localization.
    ///
    /// - Returns: The user's preferred language.
    final class func deviceLanguage() -> ChangeNowSDK.Localization.Language? {
        guard let preferredLocalization = Bundle.main.preferredLocalizations.first,
            preferredLocalization != Constants.englishLocalization,
              let preferredLanguage = ChangeNowSDK.Localization.Language(rawValue: preferredLocalization) else {
                return nil
        }

        return preferredLanguage
    }
}
