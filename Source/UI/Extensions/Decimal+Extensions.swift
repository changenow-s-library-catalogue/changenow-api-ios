//
//  Decimal+Extensions.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 26.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

extension Decimal {
    var currencyValue: String {
        if self == 0 {
            return "0"
        } else if self < 0.0000001 {
            return "< 0.0000001"
        }
        return "\(self.rounding(withMode: .down, scale: 8))"
    }
    
    func rounding(withMode mode: RoundingMode, scale: Int) -> Decimal {
        let rounder = NSDecimalNumberHandler(roundingMode: mode,
                                             scale: Int16(scale),
                                             raiseOnExactness: false,
                                             raiseOnOverflow: false,
                                             raiseOnUnderflow: false,
                                             raiseOnDivideByZero: false)

        return (self as NSDecimalNumber).rounding(accordingToBehavior: rounder) as Decimal
    }
}
