//
//  String+Extensions.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 30.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

extension Character {

    func isDigit() -> Bool {
        return isWholeNumber
    }

    func isZeroDigit() -> Bool {
        return ("\(self)" as NSString).integerValue == 0
    }
}

extension String {
    var floatDigit: String {
        var isDotted = false
        var isZeroStart = true
        var floatDigit = ""
        for (index, character) in self.enumerated() {
            if character == "." {
                if index == 0 {
                    isZeroStart = false
                    floatDigit.append("0")
                }
                if !isDotted {
                    isDotted = true
                    floatDigit.append(character)
                }
            } else {
                if isZeroStart {
                    if !character.isZeroDigit() || index != 0 {
                        isZeroStart = false
                    }
                    if index != 0, !isDotted {
                        isDotted = true
                        floatDigit.append(".")
                    }
                }
                floatDigit.append(character)
            }
        }
        return floatDigit
    }
    
    var nonEmpty: String? {
        if self.isEmpty {
            return nil
        } else {
            return self
        }
    }
}
