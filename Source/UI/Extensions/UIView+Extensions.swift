//
//  UIView+Extensions.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 26.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import UIKit

extension UIView {
    func makeConstraints(_ constraintsClouser: (UIView) -> [NSLayoutConstraint]) {
        translatesAutoresizingMaskIntoConstraints = false
        constraintsClouser(self).forEach {
            $0.isActive = true
        }
    }
}
