//
//  CharacterSet+Extensions.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 30.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

extension CharacterSet {

    static let hexadecimal = CharacterSet(charactersIn: "0123456789ABCDEFabcdef")

    static let latinLetters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

    static let addressLetters: CharacterSet = {
        var characterSet = latinLetters
        characterSet.formUnion(CharacterSet(charactersIn: ":1234567890"))
        return characterSet
    }()

    static let dot = CharacterSet(charactersIn: ".")

    static let floatDigits: CharacterSet = {
        var characterSet = CharacterSet.dot
        characterSet.formUnion(.decimalDigits)
        return characterSet
    }()
}
