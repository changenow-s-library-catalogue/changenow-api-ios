//
//  Collection+Extensions.swift
//  ChangeNow-SDK
//
//  Created by Mikhail Shemin on 28.04.2022.
//  Copyright © 2022 ChangeNow Limited. All rights reserved.
//

import Foundation

extension Collection {

    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
