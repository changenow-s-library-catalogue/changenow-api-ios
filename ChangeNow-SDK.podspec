Pod::Spec.new do |s|
  # Version
  s.version       = "1.0.0"
  s.swift_version = "5.0"
  s.ios.deployment_target = '11.0'

  # Meta
  s.name = "ChangeNow-SDK"
  s.summary = "exchange API wrapper"
  s.homepage = "https://gitlab.com/changenow-s-library-catalogue/changenow-api-ios"
  s.license = "GNU GPLv3"
  s.authors = { 'Mikhail Shemin' => 'mikhail.s@changenow.io' }
  s.description = <<-DESC
                    Integrate the ability to exchange cryptocurrencies into your application.
                  DESC

  # Compatibility & Sources
  s.source = { :git => "https://gitlab.com/changenow-s-library-catalogue/changenow-api-ios.git", :tag => s.version.to_s }
  
  
  # Subspecs
  
  s.subspec "Base" do |base|
      base.source_files = 'Source/ChangeNowSDK.swift'
  end
  
  s.subspec 'API' do |api|
      api.source_files = 'Source/API/*.swift', 'Source/API/**/*.swift'
      api.dependency 'ChangeNow-SDK/Base'
  end
  
  s.subspec 'UI' do |ui|
      ui.source_files = 'Source/UI/*.swift', 'Source/UI/**/*.swift'
      ui.resources = 'Source/UI/ChangeNowUISDK.bundle', 'Source/UI/Assets.xcassets'
      ui.dependency 'SVGKit', '~> 3.0'
      ui.dependency 'NVActivityIndicatorView', '4.8.0'
      ui.dependency 'ChangeNow-SDK/API'
      ui.dependency 'ChangeNow-SDK/Base'
  end
end
