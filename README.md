<img src="/widget_cover.png" alt="" />

# IOS exchange SDK ([changenow.io](https://changenow.io/))
This SDK is designed to quickly integrate cryptocurrency domains into your application.

## Installation

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```


To integrate ChangeNow-SDK into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '11.0'
use_frameworks!

target '<Your Target Name>' do
    pod 'ChangeNow-SDK', '~> 1.0.0'
end
```

>Use ```pod 'ChangeNow-SDK/API', '~> 1.0.0'``` to integrate only api wrapper


Then, run the following command:

```bash
$ pod install
```

### Swift Package Manager

[Swift Package Manager](https://swift.org/package-manager/) is a tool for managing the distribution of Swift code. It’s integrated with the Swift build system to automate the process of downloading, compiling, and linking dependencies.

> Xcode 11+ is required to build ChangeNow-SDK using Swift Package Manager.

To integrate ChangeNow-SDK into your Xcode project using Swift Package Manager, add it to the dependencies value of your `Package.swift`:

```swift
dependencies: [
    .package(url: "https://gitlab.com/changenow-s-library-catalogue/changenow-api-ios.git", .upToNextMajor(from: "1.0.0"))
]
```
>Select ```ChangeNow-SDK/API``` to integrate only api wrapper

### Manually

If you prefer not to use either of the aforementioned dependency managers, you can integrate ChangeNow-SDK into your project manually.

## Usage

### Api key
You can request a free key on the page [Exchange API](https://changenow.io/api/docs)

### ChangeNow-SDK/API

Wrap over [Exchange API](https://changenow.io/api/docs)
```swift
import ChangeNow_SDK

class MyViewController: UIViewController {

    let service = CNApiService(apiKey: "Your api key")

    override func viewDidLoad() {
        super.viewDidLoad()

        service.fetchAvailableCurrencies(onlyActive: true) { result in
                switch result {
                case .success(let currencies):
                    
                case .failure(let error):
                   
                }
        }
    }
}
```
---
### ChangeNow-SDK
UI wrapper over ```CNApiService```

* **CNCalculatorView** -
The main view that passes data for calculation to ``ExchangeManager``.
You can enable / disable the input of the receiving address.
*  **CNExchangeDefaultManager** - 
Default manager for processing user input and calculating the exchange. Handles matching the minimum maximum value
For error handling use `var errorHandler: ((ErrorPlace, CNNetworkError) -> Void)?`
`getExchangePrepareData()` is used to return the precalculation
*  **CNCurrenciesManager**
Used to query and cache cryptocurrencies
*  **SelectCurrencyViewController**
Default `ViewController` for choosing a cryptocurrency for exchange
```swift
import ChangeNow_SDK

class MyViewController: UIViewController {

    enum Direction {
        case from
        case to
        
        var title: String {
            switch self {
            case .from:
                return "You send"
            case .to:
                return "You get"
            }
        }
    }

    let apiService = CNApiService(apiKey: "Your api key")
    
    lazy var currenciesManager = CNCurrenciesManager(apiService: apiService)
    
    lazy var exchangeManager: CNExchangeDefaultManager = {
        let view = CNExchangeDefaultManager(configuration: .init(apiService: apiService))
        
        view.selectSendCurrencyAction = { [weak self] selectedCurrency in
           self?.showSelectCurrency(direction: .from, selectedCurrency: selectedCurrency)
        }
        
        view.selectGetCurrencyAction = { [weak self] selectedCurrency in
           self?.showSelectCurrency(direction: .to, selectedCurrency: selectedCurrency)
        }
        
        
        view.errorHandler = { (place, error) in
            ...
        }
        
        return view
    }()
    
    lazy var calculatorView = CNCalculatorView(frame: .zero, exchangeManager: exchangeManager, showAddressInputs: true)

    override func viewDidLoad() {
        super.viewDidLoad()

        
        view.addSubview(calculatorView)
    }
    
    func showSelectCurrency(direction: Direction, selectedCurrency: CurrencyType) {
        let vc = CNSelectCurrencyViewController(currenciesManager: self.currenciesManager, selectedCurrency: selectedCurrency, presentationType: .present(title: direction.title))
        vc.selectCurrencyAction = { [weak self, weak vc] newCurrency in
            switch direction {
            case .from:
                self?.exchangeManager.sendCurrency = newCurrency
            case .to:
                self?.exchangeManager.getCurrency = newCurrency
            }
            vc?.dismiss(animated: true)
        }
        present(vc, animated: true, completion: nil)
    }
}
```

## Sample app
See how to use the the library in your project:

[Example for ChangeNow-SDK/API](/Examples/API)

[Example for ChangeNow-SDK](/Examples/UI)


<img src="/Examples/UI/UI1.png" width="200" height="432" /> <img src="/Examples/UI/UI2.png" width="200" height="432"  />


Run the following command in example folders and set api key:
```bash
$ pod install
```

## Any questions
If you would like to contribute code you can do so through GitHub by forking the repository and sending a pull request.
If you have any question create an issue.
## License
Library are licensed under the [GPL-3.0 License](LICENSE).
Enjoy!

