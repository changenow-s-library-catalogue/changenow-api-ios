// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "changenow-api-ios",
    platforms: [.iOS(.v11)],
    products: [
        .library(name: "ChangeNow-SDK", targets: ["ChangeNow-SDK"]),
        .library(name: "ChangeNow-SDK/API", targets: ["ChangeNow-SDK-API"]),
    ],
    dependencies: [
        .package(url: "https://github.com/SVGKit/SVGKit.git", from: "3.0.0"),
        .package(url: "https://github.com/ninjaprox/NVActivityIndicatorView.git", "4.8.0"..."4.8.0")
    ],
    targets: [
        .target(name: "Base",
                path: "Source",
                exclude: ["API", "UI"],
                sources: ["ChangeNowSDK.swift"]),
        
        .target(name: "ChangeNow-SDK-API",
                dependencies: ["Base"],
                path: "Source/API"),
        
        .target(name: "ChangeNow-SDK",
                dependencies: ["Base", "ChangeNow-SDK-API", "SVGKit", "NVActivityIndicatorView"],
                path: "Source/UI",
                resources: [.copy("ChangeNowUISDK.bundle"), .process("Assets.xcassets")])
    ],
    swiftLanguageVersions: [.v5]
)
