//
//  Extansions.swift
//  api-exemple
//
//  Created by Mikhail Shemin on 21.04.2022.
//

import UIKit

let positionalHoursMinutesFormatter: DateComponentsFormatter = {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.hour, .minute]
    formatter.unitsStyle = .positional
    formatter.zeroFormattingBehavior = .pad
    return formatter
}()

let positionalMinutesSecondsFormatter: DateComponentsFormatter = {
    let formatter = DateComponentsFormatter()
    formatter.allowedUnits = [.minute, .second]
    formatter.unitsStyle = .positional
    formatter.zeroFormattingBehavior = .pad
    return formatter
}()

extension Decimal {
    var currencyValue: String {
        if self == 0 {
            return "0"
        } else if self < 0.0000001 {
            return "< 0.0000001"
        }
        return "\(self.rounding(withMode: .down, scale: 8))"
    }
    
    func rounding(withMode mode: RoundingMode, scale: Int) -> Decimal {
        let rounder = NSDecimalNumberHandler(roundingMode: mode,
                                             scale: Int16(scale),
                                             raiseOnExactness: false,
                                             raiseOnOverflow: false,
                                             raiseOnUnderflow: false,
                                             raiseOnDivideByZero: false)

        return (self as NSDecimalNumber).rounding(accordingToBehavior: rounder) as Decimal
    }
}

extension UIView {
    var safeIsHidden: Bool {
        get {
            self.isHidden
        }
        set {
            if self.isHidden != newValue {
                self.isHidden = newValue
            }
        }
    }
}

extension Collection {

    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension UIViewController {
    func showError(_ errorMessage: String?) {
        
        guard let errorMessage = errorMessage else { return }
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
