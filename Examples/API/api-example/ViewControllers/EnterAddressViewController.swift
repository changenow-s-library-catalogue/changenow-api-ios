//
//  EnterAddressViewController.swift
//  api-exemple
//
//  Created by Mikhail Shemin on 21.04.2022.
//

import ChangeNow_SDK
import SnapKit
import UIKit
import WebKit

class EnterAddressViewController: UIViewController {
    
    let service = CNApiService(apiKey: Constants.apiKey)
    
    private lazy var textFieldsStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [addressLabel, addressTextField, extraIdLabel, extraIdTextField])
        view.spacing = 16
        view.setCustomSpacing(8, after: addressLabel)
        view.setCustomSpacing(8, after: extraIdLabel)
        view.axis = .vertical
        return view
    }()
    
    private lazy var addressLabel: UILabel = {
        let view = UILabel()
        view.text = "Address or fio:"
        view.textColor = .gray
        view.font = .systemFont(ofSize: 12, weight: .regular)
        return view
    }()

    private lazy var addressTextField: UITextField = {
        let view = UITextField()
        view.backgroundColor = .white
        view.borderStyle = .roundedRect
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var extraIdLabel: UILabel = {
        let view = UILabel()
        view.text = "Extra Id (optional):"
        view.textColor = .gray
        view.font = .systemFont(ofSize: 12, weight: .regular)
        view.isHidden = !toCurrency.hasExternalId
        return view
    }()

    private lazy var extraIdTextField: UITextField = {
        let view = UITextField()
        view.backgroundColor = .white
        view.borderStyle = .roundedRect
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        view.isHidden = !toCurrency.hasExternalId
        return view
    }()
    
    private lazy var calculatedView: CalculatedView = {
        let view = CalculatedView(showButton: false)
        view.set(model: estimatedExchange)
        return view
    }()
    
    private lazy var startExchangeButton: LoadingButton = {
        let view = LoadingButton()
        view.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        view.setTitle("Start exchange", for: .normal)
        view.backgroundColor = UIColor(red: 0, green: 194/255.0, blue: 111/255.0, alpha: 1)
        view.layer.cornerRadius = 12
        view.addTarget(self, action: #selector(validateAndStartExchange), for: .touchUpInside)
        return view
    }()
    
    private let toCurrency: CNCurrency
    private let estimatedExchange: CNEstimatedExchange
    
    init(toCurrency: CNCurrency, estimatedExchange: CNEstimatedExchange) {
        self.toCurrency = toCurrency
        self.estimatedExchange = estimatedExchange
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        setViews()
        setConstraints()
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboatd))
        tapRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapRecognizer)
    }
    
    private func setViews() {
        view.addSubview(textFieldsStackView)
        view.addSubview(calculatedView)
        view.addSubview(startExchangeButton)
    }
    
    private func setConstraints() {
        textFieldsStackView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(16)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        
        calculatedView.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.bottom.equalTo(startExchangeButton.snp.top).inset(-16)
        }
        startExchangeButton.snp.makeConstraints {
            $0.height.equalTo(48)
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(16)
        }
    }
    
    private func showErrorIfNeeded(error: CNNetworkError) {
        let errorMessage: String?
        switch error {
        case .api(let apiError):
            errorMessage = apiError.message ?? "Unown"
        case .underlying(let underlyingError), .jsonDecoder(let underlyingError):
            errorMessage = underlyingError.localizedDescription
        case .unown, .emptyResult:
            errorMessage = nil
        }
        
        guard let errorMessage = errorMessage else { return }
        showError(errorMessage)
    }
    
    private func startExchang(address: String) {
        startExchangeButton.showLoading()
        
        let amount = estimatedExchange.type == .direct ? estimatedExchange.fromAmount : estimatedExchange.toAmount
        
        service.createExchange(fromCurrency: estimatedExchange.fromCurrency, toCurrency: estimatedExchange.toCurrency, fromNetwork: estimatedExchange.fromNetwork, toNetwork: estimatedExchange.toNetwork, address: address, addressExtraId: extraIdTextField.text, flow: estimatedExchange.flow, type: estimatedExchange.type, amount: amount, rateId: estimatedExchange.rateId) { [weak self] result in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.startExchangeButton.hideLoading()
                
                switch result {
                case .success(let txInfo):
                    let url = Constants.changeNowTxsUrl + txInfo.id
                    let webViewController = WebViewController(url: URL(string: url)!)
                    self.navigationController?.pushViewController(webViewController, animated: true)
                case .failure(let error):
                    self.showErrorIfNeeded(error: error)
                }
            }
        }
    }
    
    private func isFIO(address: String) -> Bool {
        let regEx = "^(?:(?=.{3,64}$)[a-zA-Z0-9]{1}(?:(?!-{2,}))[a-zA-Z0-9-]*(?:(?<!-))@[a-zA-Z0-9]{1}(?:(?!-{2,}))[a-zA-Z0-9-]*(?:(?<!-))$)"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regEx)
        return predicate.evaluate(with: address)
    }
    
    @objc private func validateAndStartExchange() {
        let rawAddress = addressTextField.text
         ?? ""
        
        guard !rawAddress.isEmpty else {
            showError("Enter address")
            return
        }
        
        startExchangeButton.showLoading()
        
        if isFIO(address: rawAddress) {
            service.userAddressesByName(name: rawAddress) { [weak self] result in
                guard let self = self else { return }
                DispatchQueue.main.async {
                    self.startExchangeButton.hideLoading()
                    switch result {
                    case .success(let fioResult):
                        guard fioResult.success else {
                            self.showError(fioResult.message ?? "FIO not found")
                            return
                        }
                        if let address = fioResult.addresses.first(where: { $0.currency.lowercased() == self.toCurrency.legacyTicker.lowercased() }) {
                            self.startExchang(address: address.address)
                        } else {
                            self.showError("Not found \(self.toCurrency.network) address for \(rawAddress)")
                        }
                    case .failure(let error):
                        self.showErrorIfNeeded(error: error)
                    }
                }
            }
        } else {
            service.addressValidation(currency: toCurrency, address: rawAddress) { [weak self] result in
                guard let self = self else { return }
                
                DispatchQueue.main.async {
                    self.startExchangeButton.hideLoading()
                    
                    switch result {
                    case .success(let validationResult):
                        if validationResult.result {
                            self.startExchang(address: rawAddress)
                        } else {
                            self.showError(validationResult.message ?? "Invalid address")
                        }
                    case .failure(let error):
                        self.showErrorIfNeeded(error: error)
                    }
                }
            }
        }
    }
    
    @objc private func hideKeyboatd() {
        self.view.endEditing(false)
    }
}
