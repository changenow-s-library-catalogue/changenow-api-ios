//
//  ExchangeViewController.swift
//  api-exemple
//
//  Created by Mikhail Shemin on 28.03.2022.
//

import UIKit
import SnapKit
import ChangeNow_SDK

class ExchangeViewController: UIViewController {
    
    let service = CNApiService(apiKey: Constants.apiKey)
    
    
    //MARK: - Tikers buttons view
    private lazy var tikersButtonsView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [fromTikerButton, toTikerButton])
        view.axis = .horizontal
        view.spacing = 16
        view.distribution = .fillEqually
        return view
    }()
    
    private lazy var fromTikerButton: UIButton = {
        let view = UIButton()
        view.setTitle("Send Tiker", for: .normal)
        view.backgroundColor = UIColor(red: 0, green: 194/255.0, blue: 111/255.0, alpha: 1)
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(tapOnSelectCurrency), for: .touchUpInside)
        return view
    }()
    
    private lazy var toTikerButton: UIButton = {
        let view = UIButton()
        view.setTitle("Get Tiker", for: .normal)
        view.backgroundColor = UIColor(red: 0, green: 194/255.0, blue: 111/255.0, alpha: 1)
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(tapOnSelectCurrency), for: .touchUpInside)
        return view
    }()
    
    //MARK: - FixRate view
    private lazy var fixRateView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [fixRateLabel, fixRateSwither])
        view.axis = .horizontal
        view.spacing = 16
        return view
    }()
    private lazy var fixRateLabel: UILabel = {
        let view = UILabel()
        view.text = "Use fix rate"
        view.textColor = .black
        view.font = .systemFont(ofSize: 12, weight: .medium)
        return view
    }()
    
    private lazy var fixRateSwither: UISwitch = {
        let view = UISwitch()
        view.setOn(isFixRate, animated: false)
        view.addTarget(self, action: #selector(fixRateSwitchPressed), for: .valueChanged)
        return view
    }()
    
    //MARK: - Flow view
    private lazy var flowView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [flowLabel, flowSegmentedControl])
        view.axis = .horizontal
        view.alignment = .center
        view.spacing = 16
        view.isHidden = !isFixRate
        return view
    }()
    
    private lazy var flowLabel: UILabel = {
        let view = UILabel()
        view.text = "Flow"
        view.textColor = .black
        view.font = .systemFont(ofSize: 12, weight: .medium)
        return view
    }()
    
    private lazy var flowSegmentedControl: UISegmentedControl = {
        let view = UISegmentedControl(items: [CNExchangeType.direct.rawValue,
                                              CNExchangeType.reverse.rawValue])
        view.selectedSegmentIndex = 0
        view.addTarget(self, action: #selector(changeFlowSegmente), for: .valueChanged)
        return view
    }()
    
    //MARK: - Amount view
    private lazy var amountView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [amountLabel, amountTextField])
        view.axis = .vertical
        view.spacing = 8
        return view
    }()
    
    private lazy var amountLabel: UILabel = {
        let view = UILabel()
        view.text = "Amount: "
        view.textColor = .gray
        view.font = .systemFont(ofSize: 12, weight: .regular)
        return view
    }()

    private lazy var amountTextField: UITextField = {
        let view = UITextField()
        view.keyboardType = .decimalPad
        view.backgroundColor = .white
        view.borderStyle = .roundedRect
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        return view
    }()
    
    private lazy var calculatedView: CalculatedView = {
        let view = CalculatedView()
        view.isHidden = true
        view.tapOnEnterAddressEvent = { [weak self] in
            guard let self = self,
                  let toCurrency = self.toCurrency,
                  let lastEstimatedExchange = self.lastEstimatedExchange else {
                return
            }
            let enterAddressVC = EnterAddressViewController(toCurrency: toCurrency, estimatedExchange: lastEstimatedExchange)
            self.navigationController?.pushViewController(enterAddressVC, animated: true)
        }
        return view
    }()
    
    private lazy var calculateButton: LoadingButton = {
        let view = LoadingButton()
        view.titleLabel?.font = .systemFont(ofSize: 22, weight: .medium)
        view.setTitle("Calculate", for: .normal)
        view.backgroundColor = UIColor(red: 0, green: 194/255.0, blue: 111/255.0, alpha: 1)
        view.layer.cornerRadius = 12
        view.addTarget(self, action: #selector(calculateExchange), for: .touchUpInside)
        return view
    }()
    
    
    //MARK: - Properties
    private var currencies: [CNCurrency] = []
    
    private var toCurrency: CNCurrency? {
        didSet {
            if let toCurrency = toCurrency {
                toTikerButton.setTitle(toCurrency.nameForShow, for: .normal)
            } else {
                toTikerButton.setTitle("Get Tiker", for: .normal)
            }
        }
    }
    private var fromCurrency: CNCurrency? {
        didSet {
            if let fromCurrency = fromCurrency {
                fromTikerButton.setTitle(fromCurrency.nameForShow, for: .normal)
            } else {
                fromTikerButton.setTitle("Send Tiker", for: .normal)
            }
        }
    }
    
    private var amount: Decimal = 0
    private var address: String = ""
    private var isFixRate = false {
        didSet {
            if isFixRate {
                flowView.isHidden = false
            } else {
                flowView.isHidden = true
                flowSegmentedControl.selectedSegmentIndex = 0
                flowSegmentedControl.sendActions(for: .valueChanged)
            }
        }
    }
    private var exchangeType: CNExchangeType = .direct
    
    private var lastEstimatedExchange: CNEstimatedExchange?
    private var lastEstimateDataTask: URLSessionDataTask?

    //MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        service.fetchAvailableCurrencies(onlyActive: true) { [weak self] result in
            DispatchQueue.main.async {
                guard let self = self else { return }
                switch result {
                case .success(let currencies):
                    self.currencies = currencies
                case .failure(let error):
                    self.showErrorIfNeeded(error: error)
                }
            }
        }
        
        setViews()
        setConstraints()
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboatd))
        tapRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapRecognizer)
    }

    private func setViews() {
        view.addSubview(tikersButtonsView)
        view.addSubview(fixRateView)
        view.addSubview(flowView)
        view.addSubview(amountView)
        view.addSubview(calculatedView)
        view.addSubview(calculateButton)
    }
    
    private func setConstraints() {
        tikersButtonsView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(16)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        fixRateView.snp.makeConstraints {
            $0.top.equalTo(tikersButtonsView.snp.bottom).inset(-16)
            $0.leading.equalToSuperview().inset(16)
        }
        flowView.snp.makeConstraints {
            $0.centerY.equalTo(fixRateView.snp.centerY)
            $0.leading.equalTo(fixRateView.snp.trailing).inset(-32)
        }
        amountView.snp.makeConstraints {
            $0.top.equalTo(fixRateView.snp.bottom).inset(-16)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        calculatedView.snp.makeConstraints {
            $0.top.equalTo(amountView.snp.bottom).inset(-32)
            $0.leading.trailing.equalToSuperview().inset(16)
        }
        calculateButton.snp.makeConstraints {
            $0.height.equalTo(48)
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(16)
        }
    }

    private func showErrorIfNeeded(error: CNNetworkError) {
        let errorMessage: String?
        switch error {
        case .api(let apiError):
            errorMessage = apiError.message ?? "Unown"
        case .underlying(let underlyingError), .jsonDecoder(let underlyingError):
            errorMessage = underlyingError.localizedDescription
        case .unown, .emptyResult:
            errorMessage = nil
        }
        
        guard let errorMessage = errorMessage else { return }
        showError(errorMessage)
    }
    
    @objc private func tapOnSelectCurrency(_ sender: UIButton) {
        let newVC = SelectCurrencyViewController(currencies: currencies)
        newVC.modalPresentationStyle = .pageSheet
        switch sender {
        case fromTikerButton:
            newVC.selectAction = { [weak self, weak newVC] currency in
                self?.fromCurrency = currency
                newVC?.dismiss(animated: true)
            }
        case toTikerButton:
            newVC.selectAction = { [weak self, weak newVC] currency in
                self?.toCurrency = currency
                newVC?.dismiss(animated: true)
            }
        default:
            assertionFailure("unown UIButton")
        }
        self.present(newVC, animated: true)
    }
    
    @objc private func fixRateSwitchPressed(_ sender: UISwitch) {
        isFixRate = sender.isOn
    }
    
    @objc private func changeFlowSegmente(_ sender: UISegmentedControl) {
        
        switch sender.selectedSegmentIndex {
        case 0:
            exchangeType = .direct
        case 1:
            exchangeType = .reverse
        default:
            assertionFailure("unown index")
        }
    }
    
    @objc private func calculateExchange() {
        
        guard let toCurrency = toCurrency,
              let fromCurrency = fromCurrency,
              let amount = Decimal(string: amountTextField.text?.replacingOccurrences(of: ",", with: ".") ?? "") else {
            showError("Check all fields for correctness")
            return
        }
        lastEstimateDataTask?.cancel()
        calculateButton.showLoading()
        lastEstimateDataTask = service.fetchEstimatedExchangeAmount(fromCurrency: fromCurrency,
                                                                    toCurrency: toCurrency,
                                                                    flow: isFixRate ? .fixedRate : .standard,
                                                                    type: exchangeType,
                                                                    amount: amount,
                                                                    useRateId: isFixRate) { [weak self] result in
            DispatchQueue.main.async {
                self?.calculateButton.hideLoading()
                switch result {
                case .success(let estimatedExchange):
                    self?.lastEstimatedExchange = estimatedExchange
                    self?.calculatedView.isHidden = false
                    self?.calculatedView.set(model: estimatedExchange)
                case .failure(let error):
                    if case .api(let apiError) = error {
                        self?.calculatedView.isHidden = false
                        if let errorMessage = apiError.message {
                            self?.calculatedView.set(error: errorMessage)
                            return
                        }
                        if let errorType = apiError.errorType {
                            if errorType == "deposit_too_small" {
                                self?.calculatedView.set(error: "Amount is too small")
                            }
                        }
                    } else {
                        self?.showErrorIfNeeded(error: error)
                    }
                }
            }
        }
    }
    
    @objc private func hideKeyboatd() {
        self.view.endEditing(false)
    }
}

extension CNCurrency {
    var nameForShow: String {
        if ticker != network {
            return "\(ticker.uppercased())(\(network.lowercased()))"
        } else {
            return ticker.uppercased()
        }
    }
}

