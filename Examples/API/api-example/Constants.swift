//
//  Constants.swift
//  api-example
//
//  Created by Mikhail Shemin on 05.05.2022.
//

import Foundation

struct Constants {
    static let apiKey = "your api key"
    static let changeNowTxsUrl = "https://changenow.io/exchange/txs/"
}
