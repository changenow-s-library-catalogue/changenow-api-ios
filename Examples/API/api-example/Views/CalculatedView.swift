//
//  CalculatedView.swift
//  api-exemple
//
//  Created by Mikhail Shemin on 20.04.2022.
//

import UIKit
import SnapKit
import ChangeNow_SDK

class CalculatedView: UIView {
    
    private lazy var contentStackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [fromAmountLabel, toAmountLabel, rateLabel, timerLabel, enterAddressButton, errorLabel])
        view.spacing = 8
        view.axis = .vertical
        return view
    }()
    
    private lazy var fromAmountLabel: UILabel = {
        let view = UILabel()
        view.textColor = .black
        return view
    }()
    
    private lazy var toAmountLabel: UILabel = {
        let view = UILabel()
        view.textColor = .black
        return view
    }()
    
    private lazy var rateLabel: UILabel = {
        let view = UILabel()
        view.textColor = .black
        return view
    }()
    
    private lazy var timerLabel: UILabel = {
        let view = UILabel()
        view.textColor = .black
        return view
    }()
    
    private lazy var enterAddressButton: UIButton = {
        let view = UIButton()
        view.layer.cornerRadius = 8
        view.clipsToBounds = true
        view.backgroundColor = .blue
        view.setTitle("Enter address", for: .normal)
        view.addTarget(self, action: #selector(tapOnEnterAddress), for: .touchUpInside)
        return view
    }()
    
    private lazy var errorLabel: UILabel = {
        let view = UILabel()
        view.textColor = .black
        view.numberOfLines = 0
        return view
    }()
    
    private var validUntilDate: Date? {
        didSet {
            if oldValue != validUntilDate {
                makeTick()
            }
        }
    }
    private var tickWorkItem: DispatchWorkItem?
    private let showButton: Bool
    var tapOnEnterAddressEvent: (() -> Void)?
    
    init(showButton: Bool = true) {
        self.showButton = showButton
        super.init(frame: .zero)

        layer.cornerRadius = 16
        clipsToBounds = true
        
        addSubview(contentStackView)
        contentStackView.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(16)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(model: CNEstimatedExchange) {
        backgroundColor = .init(red: 0, green: 194.0/255.0, blue: 111.0/255.0, alpha: 0.2)
        
        fromAmountLabel.safeIsHidden = false
        toAmountLabel.safeIsHidden = false
        rateLabel.safeIsHidden = false
        enterAddressButton.safeIsHidden = showButton ? false : true
        errorLabel.safeIsHidden = true
        
        fromAmountLabel.text = "You send: \(model.fromAmount.currencyValue) \(model.fromCurrencyForShow)"
        toAmountLabel.text = "You get: \(model.toAmount.currencyValue) \(model.toCurrencyForShow)"
        rateLabel.text = "1 \(model.fromCurrencyForShow) = \((model.toAmount / model.fromAmount).currencyValue) \(model.toCurrencyForShow)"
        validUntilDate = model.validUntilDate
    }
    
    func set(error: String) {
        backgroundColor = .init(red: 244.0/255.0, green: 40.0/255.0, blue: 65.0/255.0, alpha: 0.2)

        fromAmountLabel.safeIsHidden = true
        toAmountLabel.safeIsHidden = true
        rateLabel.safeIsHidden = true
        enterAddressButton.safeIsHidden = true
        timerLabel.safeIsHidden = true
        errorLabel.safeIsHidden = false
        validUntilDate = nil
        
        errorLabel.text = error
    }
    
    private func makeTick() {
        guard let validUntilDate = validUntilDate else {
            timerLabel.safeIsHidden = true
            return
        }
        timerLabel.safeIsHidden = false
        
        let duration: TimeInterval = validUntilDate.timeIntervalSince(Date())
        timerLabel.text = "Time to expiration: " + (TimeFormatter.fromSecondsToMMSS(value: Int(duration)) ?? "")
        guard duration > 0 else {
            timerLabel.text = "Expired"
            enterAddressButton.safeIsHidden = true
            return
        }
        
        let tickWorkItem = DispatchWorkItem { [weak self] in
            self?.makeTick()
        }
        self.tickWorkItem?.cancel()
        self.tickWorkItem = tickWorkItem
        
        DispatchQueue.main.asyncAfter(deadline: .now() + duration.truncatingRemainder(dividingBy: 1), execute: tickWorkItem)
    }
    
    @objc private func tapOnEnterAddress() {
        tapOnEnterAddressEvent?()
    }
}

extension CNEstimatedExchange {
    var toCurrencyForShow: String {
        if toCurrency != toNetwork {
            return "\(toCurrency.uppercased())(\(toNetwork.lowercased()))"
        } else {
            return toCurrency.uppercased()
        }
    }
    var fromCurrencyForShow: String {
        if fromCurrency != fromNetwork {
            return "\(fromCurrency.uppercased())(\(fromNetwork.lowercased()))"
        } else {
            return fromCurrency.uppercased()
        }
    }
}

struct TimeFormatter {

    /// HH:mm:ss to seconds
    static func toSeconds(value: String) -> Int {
        let components = value.components(separatedBy: ":").compactMap { Int($0) }
        let hours = (components[safe: 0] ?? 0) * 3600
        let minutes = (components[safe: 1] ?? 0) * 60
        let seconds = components[safe: 2] ?? 0
        return hours + minutes + seconds
    }

    /// localized HH:mm form of seconds
    static func fromSecondsToHHMM(value: Int) -> String? {
        return positionalHoursMinutesFormatter.string(from: TimeInterval(value))
    }

    /// localized mm:ss form of seconds
    static func fromSecondsToMMSS(value: Int) -> String? {
        return positionalMinutesSecondsFormatter.string(from: TimeInterval(value))
    }
}
