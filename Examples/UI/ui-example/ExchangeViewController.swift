//
//  ExchangeViewController.swift
//  ui-example
//
//  Created by Mikhail Shemin on 05.05.2022.
//

import UIKit
import ChangeNow_SDK
import SnapKit


class ExchangeViewController: UIViewController {
    
    private let apiService = CNApiService(apiKey: "your api key")
    
    private lazy var currenciesManager: CNCurrenciesManager = {
        switch type {
        case .exchanger:
            return CNCurrenciesManager(apiService: apiService)
        case .wallet:
            return CNCurrenciesManager(apiService: apiService, config: .init(filter: .whiteList([.network("eth")])))
        }
    }()
    
    private lazy var exchangeManager: CNExchangeDefaultManager = {
        let view: CNExchangeDefaultManager
        switch type {
        case .exchanger:
            view = CNExchangeDefaultManager(configuration: .init(apiService: apiService))
        case .wallet:
            view = CNExchangeDefaultManager(configuration: .init(apiService: apiService, initSendCurrency: USDTBTCCurrency(), initGetCurrency: BTCCurrency(), initAmount: 100))
        }
        
        view.selectSendCurrencyAction = { [weak self] selectedCurrency in
            self?.showSelectCurrency(direction: .from, selectedCurrency: selectedCurrency)
        }
        
        if case .exchanger = type {
            view.selectGetCurrencyAction = { [weak self] selectedCurrency in
                self?.showSelectCurrency(direction: .to, selectedCurrency: selectedCurrency)
            }
        }
        
        view.errorHandler = { [weak self] (_, error) in
            self?.showErrorIfNeeded(error: error)
        }
        
        return view
    }()
    
    private lazy var calculatorView: CNCalculatorView = {
        let view: CNCalculatorView
        
        switch type {
        case .exchanger:
            view = CNCalculatorView(frame: .zero, exchangeManager: exchangeManager, showAddressInputs: true)
        case .wallet:
            view = CNCalculatorView(frame: .zero, exchangeManager: exchangeManager, showAddressInputs: false)
        }
        
        view.layer.cornerRadius = 8
        return view
    }()
    
    private lazy var startExchangeButton: LoadingButton = {
        let view = LoadingButton()
        view.backgroundColor = .white
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        
        view.setTitleColor(.black, for: .normal)
        view.setTitle("Start exchange", for: .normal)
        
        view.addTarget(self, action: #selector(startExchangeAction), for: .touchUpInside)
        return view
    }()
    
    private let type: ExchangeType
    
    init(type: ExchangeType) {
        self.type = type
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .black
        
        view.addSubview(calculatorView)
        view.addSubview(startExchangeButton)
        
        calculatorView.snp.makeConstraints {
            $0.top.equalTo(view.safeAreaLayoutGuide.snp.top).inset(16)
            $0.leading.trailing.equalToSuperview()
        }
        
        startExchangeButton.snp.makeConstraints {
            $0.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(16)
            $0.leading.trailing.equalToSuperview().inset(16)
            $0.height.equalTo(44)
        }
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(hideKeyboatd))
        tapRecognizer.cancelsTouchesInView = false
        view.addGestureRecognizer(tapRecognizer)
    }
    
    private func showSelectCurrency(direction: Direction, selectedCurrency: CurrencyType) {
        if let nc = self.navigationController {
            let vc = CNSelectCurrencyViewController(currenciesManager: self.currenciesManager, selectedCurrency: selectedCurrency, presentationType: .push)
            vc.title = direction.title
            vc.selectCurrencyAction = { [weak self, weak nc] newCurrency in
                switch direction {
                case .from:
                    self?.exchangeManager.sendCurrency = newCurrency
                case .to:
                    self?.exchangeManager.getCurrency = newCurrency
                }
                nc?.popViewController(animated: true)
            }
            nc.pushViewController(vc, animated: true)
        } else {
            let vc = CNSelectCurrencyViewController(currenciesManager: self.currenciesManager, selectedCurrency: selectedCurrency, presentationType: .present(title: direction.title))
            vc.selectCurrencyAction = { [weak self, weak vc] newCurrency in
                switch direction {
                case .from:
                    self?.exchangeManager.sendCurrency = newCurrency
                case .to:
                    self?.exchangeManager.getCurrency = newCurrency
                }
                vc?.dismiss(animated: true)
            }
            present(vc, animated: true, completion: nil)
        }
        
    }
    
    private func showErrorIfNeeded(error: CNNetworkError) {
        let errorMessage: String?
        switch error {
        case .api(let apiError):
            errorMessage = apiError.message ?? "Unown"
        case .underlying(let underlyingError), .jsonDecoder(let underlyingError):
            errorMessage = underlyingError.localizedDescription
        case .unown, .emptyResult:
            errorMessage = nil
        }
        
        showError(errorMessage)
    }
    
    private func startExchang(data: CNExchangePrepareData, address: String, extraId: String? = nil) {
        startExchangeButton.showLoading()
        
        apiService.createExchange(fromCurrency: data.fromTicker,
                                  toCurrency: data.toTicker,
                                  fromNetwork: data.fromNetwork,
                                  toNetwork: data.toNetwork,
                                  address: address,
                                  addressExtraId: extraId,
                                  flow: .standard,
                                  type: .direct,
                                  amount: data.fromAmount) { [weak self] result in
            guard let self = self else { return }
            
            DispatchQueue.main.async {
                self.startExchangeButton.hideLoading()
                
                switch result {
                case .success(let txInfo):
                    let url = "https://changenow.io/exchange/txs/\(txInfo.id)"
                    let webViewController = WebViewController(url: URL(string: url)!)
                    if let nc = self.navigationController {
                        nc.pushViewController(webViewController, animated: true)
                    } else {
                        self.present(webViewController, animated: true)
                    }
                case .failure(let error):
                    self.showErrorIfNeeded(error: error)
                }
            }
        }
    }
    
    func showError(_ errorMessage: String?) {
        
        guard let errorMessage = errorMessage else { return }
        let alert = UIAlertController(title: "Error", message: errorMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc private func startExchangeAction() {
        guard let data = exchangeManager.getExchangePrepareData() else {
            let alert = UIAlertController(title: "Error", message: "Incomplete data", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            present(alert, animated: true, completion: nil)
            return
        }
        
        if let defaultAddress = type.defaultAddress {
            startExchang(data: data, address: defaultAddress)
            return
        }
        
        
        if let addressOrFIO = data.address {
            startExchangeButton.showLoading()
            apiService.addressValidation(network: data.toNetwork ?? data.toTicker, address: addressOrFIO) { [weak self] result in
                guard let self = self else { return }
                
                DispatchQueue.main.async {
                    self.startExchangeButton.hideLoading()
                    
                    switch result {
                    case .success(let validationResult):
                        if validationResult.result {
                            self.startExchang(data: data, address: addressOrFIO)
                        } else {
                            self.showError(validationResult.message ?? "Invalid address")
                        }
                    case .failure(let error):
                        self.showErrorIfNeeded(error: error)
                    }
                }
            }
        } else {
            let alert = UIAlertController(title: "Error", message: "Enter address", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            present(alert, animated: true, completion: nil)
        }
    }
    
    @objc private func hideKeyboatd() {
        self.view.endEditing(false)
    }
}

extension ExchangeViewController {
    enum Direction {
        case from
        case to
        
        var title: String {
            switch self {
            case .from:
                return "You send"
            case .to:
                return "You get"
            }
        }
    }
    
    enum ExchangeType {
        case wallet
        case exchanger
        
        var defaultAddress: String? {
            switch self {
            case .wallet:
                return "bc1qqrr2nhkvhtsxmmqkr40y093nfr6hku68ek87hg"
            case .exchanger:
                return nil
            }
        }
    }
    
    struct USDTBTCCurrency: CurrencyType {
        func getIcon(_ completion: ((UIImage?) -> Void)) -> Cancalable? {
            completion(UIImage(named: "usdtBTC"))
            return nil
        }
        
        private(set) var ticker: String = "usdt"
        
        private(set) var network: String = "btc"
        
        private(set)var name: String = "Tether"
        
        private(set)var hasExternalId: Bool = false
    }
    
    struct BTCCurrency: CurrencyType {
        func getIcon(_ completion: ((UIImage?) -> Void)) -> Cancalable? {
            completion(UIImage(named: "btc"))
            return nil
        }
        
        private(set) var ticker: String = "btc"
        
        private(set) var network: String = "btc"
        
        private(set)var name: String = "Bitcoin"
        
        private(set)var hasExternalId: Bool = false
    }
}
